# Missing AppStream OARS tags

AppCenter was unable to find OARS `content_rating` tags. Please ensure that you
add these to your AppStream file and release again.

For more information, please look at the
[Open Age Rating Service website](https://hughsie.github.io/oars/).
