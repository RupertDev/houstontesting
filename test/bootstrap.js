/**
 * houston/test/bootstrap.js
 * Runs before all tests. Sets things up.
 */

// Our tests can get pretty dang complex. Up the timeout to make travis happy.
jest.setTimeout(70000)
