/**
 * houston/src/cli/utilities.ts
 * Some utilities for command line stuff
 */
import { App } from '../lib/app';
import { Config } from '../lib/config';
import { Logger } from '../lib/log';
/**
 * Sets up some boilderplate application classes based on command line args
 *
 * @param {Object} argv
 * @return {Object}
 */
export declare function setup(argv: any): {
    app: App;
    config: Config;
    logger: Logger;
};
