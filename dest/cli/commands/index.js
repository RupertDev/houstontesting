"use strict";
/**
 * houston/src/cli/commands/index.ts
 * Exports an array of all the commands we can run.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = [
    require('./build'),
    require('./ci'),
    require('./migrate'),
    require('./repo'),
    require('./seed'),
    require('./version')
];
//# sourceMappingURL=index.js.map