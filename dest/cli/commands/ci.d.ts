/**
 * houston/src/cli/commands/ci.ts
 * Tests a project with the worker. Used when a code base is local.
 */
export declare const command = "ci [directory]";
export declare const describe = "Tests a local project with the worker process";
export declare const builder: (yargs: any) => any;
export declare function handler(argv: any): Promise<void>;
