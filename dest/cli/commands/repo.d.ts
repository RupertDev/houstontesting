/**
 * houston/src/cli/commands/repo.ts
 * Runs the repository syslogd server
 */
export declare const command = "repo";
export declare const describe = "Starts the repository syslogd server";
export declare const builder: (yargs: any) => any;
export declare function handler(argv: any): Promise<void>;
