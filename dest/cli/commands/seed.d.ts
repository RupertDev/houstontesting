/**
 * houston/src/cli/commands/seed.ts
 * Runs database seed scripts
 */
export declare const command = "seed";
export declare const describe = "Seeds the database tables with fake data";
export declare function handler(argv: any): Promise<void>;
