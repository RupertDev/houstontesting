/**
 * houston/src/cli/commands/migrate.ts
 * Runs database migration scripts
 */
export declare const command = "migrate";
export declare const describe = "Changes database tables based on houston schemas";
export declare const builder: (yargs: any) => any;
export declare function handler(argv: any): Promise<void>;
