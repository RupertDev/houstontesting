/**
 * houston/src/cli/commands/build.ts
 * Builds a project with the worker
 */
export declare const command = "build <repo> <version>";
export declare const describe = "Builds a repository with the worker process";
export declare const builder: (yargs: any) => any;
export declare function handler(argv: any): Promise<void>;
