/**
 * houston/src/cli/commands/seed.ts
 * Runs database seed scripts
 */
export declare const command = "version";
export declare const describe = "Displays Houston version information";
export declare function handler(argv: any): Promise<void>;
