/**
 * houston/test/utility/http.ts
 * Utilities for when you are testing http related things
 */
export interface INockOptions {
    ignoreBody?: boolean;
}
/**
 * Sets up nock to record all API calls in the test.
 *
 * @param {String} p Path of the mock
 * @param {INockOptions} opts
 * @return {Object}
 * @return {Function} done - The function to run to stop mocking
 */
export declare function record(p: string, opts?: INockOptions): Promise<{
    done: any;
}>;
