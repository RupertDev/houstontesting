"use strict";
/**
 * houston/test/utility/fs.ts
 * Utilties for testing on the filesystem
 *
 * @exports {Function} tmp - Creates a temp directory for tests
 */
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs = require("fs-extra");
const os = require("os");
const path = require("path");
const uuid = require("uuid/v4");
/**
 * Returns full path to a fixture file
 *
 * @param {string} file - Relative to the fixture test directory
 * @return {string}
 */
function fixture(file = '') {
    return path.resolve(__dirname, '../fixture', file);
}
exports.fixture = fixture;
/**
 * tmp
 * Creates a temp directory for tests
 *
 * @async
 * @param {string} [dir] - Directory to create
 * @return {string}
 */
function tmp(dir = '') {
    return __awaiter(this, void 0, void 0, function* () {
        const directory = path.resolve(os.tmpdir(), 'houston-test', dir, uuid());
        yield fs.ensureDir(directory);
        return directory;
    });
}
exports.tmp = tmp;
//# sourceMappingURL=fs.js.map