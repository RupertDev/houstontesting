/**
 * houston/test/utility/fs.ts
 * Utilties for testing on the filesystem
 *
 * @exports {Function} tmp - Creates a temp directory for tests
 */
/**
 * Returns full path to a fixture file
 *
 * @param {string} file - Relative to the fixture test directory
 * @return {string}
 */
export declare function fixture(file?: string): string;
/**
 * tmp
 * Creates a temp directory for tests
 *
 * @async
 * @param {string} [dir] - Directory to create
 * @return {string}
 */
export declare function tmp(dir?: string): Promise<string>;
