"use strict";
/**
 * houston/test/utility/http.ts
 * Utilities for when you are testing http related things
 */
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const nock = require("nock");
const path = require("path");
nock.back.fixtures = path.resolve(__dirname, '../fixture');
/**
 * Sets up nock to record all API calls in the test.
 *
 * @param {String} p Path of the mock
 * @param {INockOptions} opts
 * @return {Object}
 * @return {Function} done - The function to run to stop mocking
 */
function record(p, opts) {
    return __awaiter(this, void 0, void 0, function* () {
        const options = {};
        if (opts != null && opts.ignoreBody === true) {
            options.before = (scope) => {
                scope.filteringRequestBody = () => '*';
            };
        }
        const { nockDone } = yield nock.back(p, options);
        return { done: nockDone };
    });
}
exports.record = record;
//# sourceMappingURL=http.js.map