"use strict";
/**
 * houston/src/lib/log/outputs/index.ts
 * A list of outputs we can send logs to
 */
Object.defineProperty(exports, "__esModule", { value: true });
var console_1 = require("./console");
exports.Console = console_1.Console;
var sentry_1 = require("./sentry");
exports.Sentry = sentry_1.Sentry;
//# sourceMappingURL=index.js.map