/**
 * houston/src/lib/log/log.ts
 * A log message with super powers.
 *
 * @exports {Class} Log - A single log line.
 */
import { Level } from './level';
import { Logger } from './logger';
/**
 * Log
 * A single log line.
 */
export declare class Log {
    /**
     * The level of the log
     *
     * @var {Level}
     */
    level: Level;
    /**
     * The log message
     *
     * @var {String|Null}
     */
    message?: string;
    /**
     * Attached data to the log
     *
     * @var {Object}
     */
    data: object;
    /**
     * An error attached to the log
     *
     * @var {Error}
     */
    error?: Error;
    /**
     * The date the log was created
     *
     * @var {Date}
     */
    protected date: Date;
    /**
     * The current logger to use for sending the log.
     *
     * @var {Logger}
     */
    protected logger: Logger;
    /**
     * Creates a new log with default values
     *
     * @param {Logger} logger
     */
    constructor(logger: Logger);
    /**
     * Sets the log level
     *
     * @param {Level} level
     *
     * @return {Log}
     */
    setLevel(level: Level): this;
    /**
     * Sets the log message
     *
     * @param {String} message
     *
     * @return {Log}
     */
    setMessage(message: string): this;
    /**
     * Sets data in the log
     *
     * @param {String} key
     * @param {*} value
     *
     * @return {Log}
     */
    setData(key: string, value: any): this;
    /**
     * A shorthand for attaching an error message to a log
     *
     * @param {Error} err
     *
     * @return {Log}
     */
    setError(err: Error): this;
    /**
     * Gets the date this log was created.
     *
     * @return {Date}
     */
    getDate(): Date;
    /**
     * Sends the log to what ever services / places it needs to be.
     *
     * @return {void}
     */
    send(): void;
}
