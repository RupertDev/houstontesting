/**
 * houston/src/lib/config/loader.ts
 * This file is responsible for loading the configuration file.
 *
 * @exports {Function} environmentToDot
 * @exports {Function} findEnvironmentConfig
 * @exports {Function} findProgramConfig
 * @exports {Function} findFileConfig
 */
import { Config } from './index';
/**
 * stringToDot
 * Transforms an environmental variable name to dot notation.
 *
 * @example `HOUSTON_LOG_LEVEL` to `log.level`
 *
 * @param {string} str - The string to transform
 * @return {string}
 */
export declare function stringToDot(str: string): string;
/**
 * getEnvironmentConfig
 * Finds all the environment variables set and returns a built config.
 *
 * @return {Config}
 */
export declare function getEnvironmentConfig(): Config;
/**
 * getProgramConfig
 * Returns a built config with program variables like git commit and package
 * version.
 *
 * @return {Config}
 */
export declare function getProgramConfig(): Config;
/**
 * getFileConfig
 * Tries to read configuration from a file.
 *
 * @param {string} [p] - The path to the file
 *
 * @throws {Error} - On 404 file not found
 * @return {Config}
 */
export declare function getFileConfig(p?: string): Config;
/**
 * getConfig
 * This creates a Config from all possible places.
 *
 * @param {string} [p] - The path to the configuration file
 *
 * @throws {Error} - On 404 file not found
 * @return {Config}
 */
export declare function getConfig(p?: string): Config;
