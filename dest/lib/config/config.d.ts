/**
 * houston/src/lib/config/config.ts
 * The application wide configuration class
 *
 * @exports {class} config - Global configuration class
 */
/**
 * Config
 * An easy to use application wide configuration class
 */
export declare class Config {
    /**
     * tree
     * The current configuration object
     *
     * @var {object}
     */
    protected tree: object;
    /**
     * immutable
     * True if we do not allow setting of values anymore
     *
     * @var {boolean}
     */
    protected immutable: boolean;
    /**
     * freeze
     * Freezes an object
     *
     * @param {object} obj - The object to freeze
     * @return {object} - A newly frozen object
     */
    protected static freeze(obj: object): object;
    /**
     * unfreeze
     * Makes an object unfrozzen and able to accept new values
     *
     * @param {object} obj - The object to unfreeze
     * @return {object} - A new unfrozzen object
     */
    protected static unfreeze(obj: object): object;
    /**
     * Create a new Config class with the given object
     *
     * @param {object} configuration - A basic object to set as config
     */
    constructor(configuration?: object);
    /**
     * get
     * Returns a configuration value
     *
     * @param {*} key - Key value for the configuration
     * @param {*} def - The default value if configuration does not exist
     * @return {*} - The stored configuration value
     */
    get(key: any, def?: any): any;
    /**
     * has
     * Returns boolean if value exists in configuration
     *
     * @param {string} key - Dot notation path of configuration value
     * @return {boolean} - True if the configuration has the value
     */
    has(key: string): boolean;
    /**
     * set
     * Sets a configuration value
     *
     * @param {string} key - Key value to store under
     * @param {*} - The configuration value to store
     * @return {Config} - The configuration after value was set
     */
    set(key: any, value: any): this;
    /**
     * merge
     * Merges an object with the current configuration
     *
     * @param {object} obj - An object to merge in configuration
     * @return {Config} - The configuration after value was set
     */
    merge(obj: object): this;
    /**
     * freeze
     * Makes the configuration immutable
     *
     * @return {Config} - The configuration after being frozen
     */
    freeze(): this;
    /**
     * unfreeze
     * Makes the configuration editable
     *
     * @return {Config} - The configuration after unfrozzen
     */
    unfreeze(): this;
}
