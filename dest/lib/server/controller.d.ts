/**
 * houston/src/lib/server/controller.ts
 * A basic interface for a server controller.
 */
import { Context } from 'koa';
import * as Router from 'koa-router';
export declare abstract class Controller {
    /**
     * The prefix for the controller.
     *
     * @var {String}
     */
    protected prefix: string;
    /**
     * The koa-router instance we will use
     *
     * @var {Router}
     */
    protected router: Router;
    /**
     * Sets up the basic router with prefixes and needed settings.
     *
     * @return {Controller}
     */
    setupRouter(): this;
    /**
     * Sets up all of the given routes with the router.
     *
     * @return {Controller}
     */
    setupRoutes(): this;
    /**
     * Returns a list of middleware / routes to run.
     *
     * @async
     * @return {IMiddleware}
     */
    middleware(): (ctx: Context, next: () => Promise<any>) => Promise<void>;
}
