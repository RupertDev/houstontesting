/**
 * houston/src/lib/server/middleware/report.ts
 * Catches and reports errors.
 */
/// <reference types="koa-router" />
import { Context } from 'koa';
import { Config } from '../../config';
/**
 * A middleware factory function for reporting errors
 *
 * @param {Config} config
 * @return {Function}
 */
export declare function report(config: Config): (ctx: Context, next: (ctx?: Context) => Promise<void>) => Promise<void>;
