/**
 * houston/src/lib/server/middleware.ts
 * A basic interface for a server controller.
 */
import { Context } from 'koa';
import * as Router from 'koa-router';
export declare abstract class Controller {
    /**
     * The prefix for the controller.
     *
     * @var {String}
     */
    protected prefix: string;
    /**
     * The koa-router instance we will use
     *
     * @var {Router}
     */
    protected router: Router;
    /**
     * Creates a new controller
     */
    constructor();
    /**
     * Sets up all of the given routes with the router.
     *
     * @return {void}
     */
    setupRoutes(): void;
    /**
     * Returns a list of middleware / routes to run.
     *
     * @async
     * @return {IMiddleware}
     */
    middleware(): (ctx: Context, next: () => void) => Promise<void>;
}
