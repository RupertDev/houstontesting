/**
 * houston/src/lib/server/error/interface.ts
 * An interface for any error able to be rendered
 */
import { Error as HttpError } from '../type';
/**
 * Makes any sort of error an HTTP handleable error.
 *
 * @param {Error} error
 * @return {HttpError}
 */
export declare function transform(e: Error): HttpError;
