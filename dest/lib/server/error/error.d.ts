/**
 * houston/src/lib/server/error/error.ts
 * A super basic easy to use http server error
 */
/// <reference types="koa-router" />
import { Context } from 'koa';
import { Error as HttpError } from '../type';
export declare class BasicHttpError extends Error implements HttpError {
    /**
     * The HTTP status code
     *
     * @var {Number}
     */
    httpStatus: number;
    /**
     * A public readable error message
     *
     * @var {String}
     */
    httpMessage: string;
    /**
     * Creates a new basic http error
     *
     * @param {Number} status
     * @param {String} message
     */
    constructor(status?: number, message?: string);
    /**
     * Renders error to an http output
     *
     * @async
     * @param {Context} ctx
     * @return {void}
     */
    httpRender(ctx: Context): Promise<void>;
}
