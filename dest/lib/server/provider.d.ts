/**
 * houston/src/lib/server/provider.ts
 * Provides the app with the needed Server classes
 */
import { ContainerModule } from 'inversify';
export declare const provider: ContainerModule;
