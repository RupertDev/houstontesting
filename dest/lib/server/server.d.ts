/**
 * houston/src/lib/server/server.ts
 * A basic HTTP web server used for various processes.
 *
 * @exports {Class} Server - An HTTP web server
 */
/// <reference types="node" />
import * as http from 'http';
import * as Koa from 'koa';
import * as Router from 'koa-router';
import { Config } from '../config';
import { Logger } from '../log';
import { Controller } from './controller';
import { Servable } from './type';
export declare type Middleware = (config: Config) => (ctx: Koa.Context, next?: () => Promise<any>) => void;
/**
 * Server
 * A basic HTTP web server
 *
 * @property {Server} server
 * @property {number} port
 *
 * @property {boolean} active
 */
export declare class Server implements Servable {
    /**
     * A basic http server. Used for low level access (mostly testing)
     *
     * @var {http.Server}
     */
    server?: http.Server;
    /**
     * The http port we will bind to
     *
     * @var {Number}
     */
    port?: number;
    /**
     * The application configuration
     *
     * @var {Config}
     */
    protected config: Config;
    /**
     * A list of controllers this server has
     *
     * @var {Controller[]}
     */
    protected controllers: Controller[];
    /**
     * A list of middlewares that will be ran on every route
     *
     * @var {Middleware[]}
     */
    protected middlewares: Middleware[];
    /**
     * A logger instance for the whole server.
     *
     * @var {Logger}
     */
    protected logger: Logger;
    /**
     * Our Koa server instance
     *
     * @var {Koa}
     */
    protected koa: Koa;
    /**
     * The koa router via koa-router
     *
     * @var {Router} router
     */
    protected router: Router;
    /**
     * Creates a new web server
     *
     * @param {Config} config - The configuration to use
     * @param {Log} log - The logger instance to use
     */
    constructor(config: Config, logger: Logger);
    /**
     * Returns true if the http server is currently active.
     *
     * @return {boolean}
     */
    readonly active: boolean;
    /**
     * listen
     * Starts web server services
     *
     * @async
     * @param {number} [port] - A port to listen on. Kept for backwards support
     *
     * @throws {Error} - When unable to listen to requested port
     * @return {Server} - An active Server class
     */
    listen(port?: number): Promise<this>;
    /**
     * close
     * Stops the HTTP server
     *
     * @async
     *
     * @throws {Error} - When the Server class is messed up
     * @return {Server} - An inactive Server class
     */
    close(): Promise<this>;
    /**
     * http
     * Returns an http server wrapping current server. Used for testing and low
     * level plugins
     *
     * @return {http.Server} - HTTP web server
     */
    http(): http.Server;
    /**
     * Adds all of the controllers to the server.
     *
     * @return {Server}
     */
    protected registerControllers(): this;
    /**
     * Adds all of the middleware functions to the server.
     *
     * @return {Server}
     */
    protected registerMiddleware(): this;
}
