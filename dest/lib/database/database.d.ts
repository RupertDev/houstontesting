/**
 * houston/src/lib/database/database.ts
 * The main database class
 *
 * @exports {Class} Database - The master database connection class
 */
import * as Knex from 'knex';
import { Config } from '../config';
import { Log } from '../log';
/**
 * Database
 * The master database connection class
 *
 * @property {Knex} knex - A knex instance for queries
 */
export declare class Database {
    knex: Knex;
    protected config: Config;
    protected log: Log;
    /**
     * Creates a Database class
     *
     * @param {Config} config - Configuration for database connection
     * @param {Log} [log] - The log instance to use for reporting
     */
    constructor(config: Config, log: Log);
}
