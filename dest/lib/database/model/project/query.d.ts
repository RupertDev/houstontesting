/**
 * houston/src/lib/database/model/project/query.ts
 * Sets up some cool methods and overwrites then function for casting to model
 */
import { Query as BaseQuery } from '../base/query';
export declare class Query extends BaseQuery {
    /**
     * Finds a project by the name_domain field
     *
     * @param {string} name - The domain name for the project
     * @return {Query}
     */
    whereNameDomain(name: string): any;
    /**
     * Finds the newest projects, that have been released at one point or another.
     * NOTE: this does not give projects with the latest releases. Phrasing.
     *
     * @return {Query}
     */
    whereNewestReleased(): any;
}
