/**
 * houston/src/lib/database/model/base/model.ts
 * A basic master model inherited by everything
 */
import { Database } from '../../database';
import { Query } from './query';
/**
 * Model
 * A basic master model to be inherited by other models
 *
 * @property {string} id - The record's ID
 *
 * @property {Date} createdAt - The date the record was created at
 * @property {Date} updatedAt - The date the record was last updated
 * @property {Date} [deletedAt] - The date the record may have been deleted
 */
export declare class Model {
    /**
     * table
     * The table name for the current model
     *
     * @var {string}
     */
    static table: string;
    id: string;
    createdAt: Date;
    updatedAt: Date;
    deletedAt?: Date | null;
    /**
     * exists
     * If this record already exists in the database
     *
     * @var {boolean}
     */
    protected exists: boolean;
    /**
     * guarded
     * All properties that should not be included when put to object or json
     *
     * @var {string[]}
     */
    protected guarded: string[];
    /**
     * createId
     * Creates a new UUID for use in the model.
     *
     * @return {string}
     */
    static createId(): string;
    /**
     * castFromDatabase
     * Takes values from the database to create a model
     *
     * @param {object} values - Values from the database
     * @return {Model}
     */
    static castFromDatabase(values: object): Model;
    /**
     * query
     * This is a super master query function so we can put data in a model
     *
     * @param {Database} database - The database to query
     * @return {Query}
     */
    static query(database: Database): Query;
    /**
     * Creates a Model class
     *
     * @param {object} [values] - Initial values to be set
     */
    constructor(values?: object);
    /**
     * isDeleted
     * Tells if the record has been soft deleted or not
     *
     * @return {boolean}
     */
    /**
    * isDeleted
    * Sets the deleted at date
    *
    * @param {boolean} value - True if the record should be deleted
    * @return {void}
    */
    isDeleted: boolean;
    /**
     * toObject
     * Transforms the current model to a plain object
     *
     * @return {object}
     */
    toObject(): {};
    /**
     * toJson
     * Transforms the current model to a json value
     *
     * @return {string}
     */
    toJson(): string;
}
