/**
 * houston/src/lib/database/model/base/query.ts
 * Sets up some cool methods and overwrites then function for casting to model
 */
import * as Builder from 'knex/lib/query/builder';
import { Database } from '../../database';
export declare class Query extends Builder {
    /**
     * A database client instance to use
     *
     * @var {Database}
     */
    protected database: Database;
    /**
     * Model to cast to when query is finished
     *
     * @var {ModelConstructor}
     */
    protected model: any;
    /**
     * Creates a new Query instance
     *
     * @param {Database} database
     */
    constructor(database: Database);
    /**
     * Sets the castFromDatabase function for the query
     *
     * @param {ModelConstructor} model
     * @return {Query}
     */
    setModel(model: any): this;
    /**
     * Sets up the query, runs it, then casts it to a model
     *
     * @return {Model|Model[]|Object|null}
     */
    then(onResolve: any, onReject: any): void;
}
