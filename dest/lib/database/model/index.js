"use strict";
/**
 * houston/src/lib/database/model/index.ts
 * A whole list of different models
 */
Object.defineProperty(exports, "__esModule", { value: true });
var model_1 = require("./project/model");
exports.Project = model_1.Model;
var model_2 = require("./release/model");
exports.Release = model_2.Model;
//# sourceMappingURL=index.js.map