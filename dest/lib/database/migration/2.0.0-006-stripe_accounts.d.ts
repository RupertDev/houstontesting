/**
 * houston/src/lib/database/migration/2.0.0-006-stripe_accounts.ts
 * The inital houston 2.0.0 migration for stripe accounts table
 *
 * @exports {Function} up - Database information for upgrading to version 2.0.0
 * @exports {Function} down - Database information for downgrading version 2.0.0
 */
import * as Knex from 'knex';
/**
 * up
 * Database information for upgrading to version 2.0.0
 *
 * @param {Object} knex - An initalized Knex package
 * @return {Promise} - A promise of database migration
 */
export declare function up(knex: Knex): Knex.SchemaBuilder;
/**
 * down
 * Database information for downgrading version 2.0.0
 *
 * @param {Object} knex - An initalized Knex package
 * @return {Promise} - A promise of successful database migration
 */
export declare function down(knex: Knex): Knex.SchemaBuilder;
