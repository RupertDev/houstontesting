"use strict";
/**
 * houston/src/lib/database/index.ts
 * Export all the things we could possibly use outside of this module.
 */
Object.defineProperty(exports, "__esModule", { value: true });
var database_1 = require("./database");
exports.Database = database_1.Database;
//# sourceMappingURL=index.js.map