/**
 * houston/src/lib/database/provider.ts
 * Provides the app with the needed Log classes
 */
import { ContainerModule } from 'inversify';
export declare const provider: ContainerModule;
