/**
 * houston/src/lib/database/seed/004-github_users.ts
 * Seeds the database
 *
 * @exports {Function} seed - Seeds the Github users table
 */
import * as Knex from 'knex';
/**
 * seed
 * Seeds the Github users table
 *
 * @param {Object} knex - An initalized Knex package
 */
export declare function seed(knex: Knex): Promise<void>;
