/**
 * houston/src/lib/database/seed/002-github_releases.ts
 * Seeds the database
 *
 * @exports {Function} seed - Seeds the Github releases table
 */
import * as Knex from 'knex';
/**
 * seed
 * Seeds the Github releases table
 *
 * @param {Object} knex - An initalized Knex package
 */
export declare function seed(knex: Knex): Promise<void>;
