/**
 * houston/src/lib/database/seed/006-stripe_accounts.ts
 * Seeds the database
 *
 * @exports {Function} seed - Seeds the Stripe accounts table
 */
import * as Knex from 'knex';
/**
 * seed
 * Seeds the Stripe accounts table
 *
 * @param {Object} knex - An initalized Knex package
 */
export declare function seed(knex: Knex): Promise<void>;
