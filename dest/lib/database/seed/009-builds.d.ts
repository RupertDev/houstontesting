/**
 * houston/src/lib/database/seed/009-builds.ts
 * Seeds the database
 *
 * @exports {Function} seed - Seeds the builds table
 */
import * as Knex from 'knex';
/**
 * seed
 * Seeds the builds table
 *
 * @param {Object} knex - An initalized Knex package
 */
export declare function seed(knex: Knex): Promise<void>;
