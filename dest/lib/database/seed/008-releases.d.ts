/**
 * houston/src/lib/database/seed/008-releases.ts
 * Seeds the database
 *
 * @exports {Function} seed - Seeds the releases table
 */
import * as Knex from 'knex';
/**
 * seed
 * Seeds the releases table
 *
 * @param {Object} knex - An initalized Knex package
 */
export declare function seed(knex: Knex): Promise<void>;
