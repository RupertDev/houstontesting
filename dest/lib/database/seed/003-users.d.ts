/**
 * houston/src/lib/database/seed/003-users.ts
 * Seeds the database
 *
 * @exports {Function} seed - Seeds the users table
 */
import * as Knex from 'knex';
/**
 * seed
 * Seeds the users table
 *
 * @param {Object} knex - An initalized Knex package
 */
export declare function seed(knex: Knex): Promise<void>;
