/**
 * houston/src/lib/database/seed/001-github_repositories.ts
 * Seeds the database
 *
 * @exports {Function} seed - Seeds the Github repositories table
 */
import * as Knex from 'knex';
/**
 * seed
 * Seeds the Github repositories table
 *
 * @param {Object} knex - An initalized Knex package
 */
export declare function seed(knex: Knex): Promise<void>;
