/**
 * houston/src/lib/database/seed/007-projects.ts
 * Seeds the database
 *
 * @exports {Function} seed - Seeds the projects table
 */
import * as Knex from 'knex';
/**
 * seed
 * Seeds the projects table
 *
 * @param {Object} knex - An initalized Knex package
 */
export declare function seed(knex: Knex): Promise<void>;
