/**
 * houston/src/lib/database/seed/005-github_repositories_github_users.ts
 * Seeds the database
 *
 * @exports {Function} seed - Seeds the Github repositories to Github users table
 */
import * as Knex from 'knex';
/**
 * seed
 * Seeds the Github repositories to Github users table
 *
 * @param {Object} knex - An initalized Knex package
 */
export declare function seed(knex: Knex): Promise<void>;
