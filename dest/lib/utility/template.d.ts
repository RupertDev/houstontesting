/**
 * houston/src/lib/utility/template.ts
 * A simple way to template strings using ejs
 *
 * @example
 * ```
 *   import template from 'lib/utility/template'
 *
 *   return template('# <%= title %>', { title: 'testing' })
 * ```
 */
declare const _default: any;
export default _default;
