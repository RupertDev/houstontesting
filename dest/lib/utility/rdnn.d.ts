/**
 * houston/src/lib/utility/rdnn.ts
 * Some higher level RDNN functions
 */
/**
 * Sanitizes an RDNN string for common mistakes and better unification
 * @see https://github.com/elementary/houston/issues/566
 *
 * @param {string} rdnn
 * @return {string}
 */
export declare function sanitize(rdnn: string): string;
