/**
 * houston/src/lib/utility/glob.ts
 * Promiseifies the glob package function
 */
/**
 * A promise-ify passthrough function for glob
 *
 * @async
 *
 * @param {String} pattern
 * @param {Object} [options]
 *
 * @return {String[]}
 */
export declare function glob(pattern: any, options?: {}): Promise<string[]>;
