/**
 * houston/src/lib/utility/markdown.ts
 * Parses markdown to an html string
 *
 * @example
 * ```
 *   import markdown from 'lib/utility/markdown'
 *
 *   return markdown('# this is a string')
 * ```
 */
/**
 * Renders a markdown string to html
 *
 * @param {String} str
 * @param {Object} [opts]
 * @return {string}
 */
export default function (str: any, opts?: {}): any;
