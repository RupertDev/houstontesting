/**
 * houston/src/lib/utility/eventemitter.ts
 * An event emitter based on eventemitter2 with some nice added features
 */
import { EventEmitter2 } from 'eventemitter2';
export declare class EventEmitter extends EventEmitter2 {
    /**
     * Creates a new event emitter
     *
     * @param {Object} [opts]
     */
    constructor(opts?: {});
    /**
     * This emites an async event, that will resolve the results by running
     * listeners step by step. This is great for things that can be extended and
     * modified by listeners.
     *
     * @param {string} event
     * @param {*} arg
     * @return {*} - Results of arg after modification from listeners
     */
    emitAsyncChain<T>(event: any, arg: any): Promise<T>;
}
