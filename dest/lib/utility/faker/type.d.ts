/**
 * houston/src/lib/utility/faker/type.ts
 * Some nice types for Faker
 */
/// <reference types="faker" />
export declare type FakerProvider = (faker: Faker.FakerStatic) => object;
export declare const fakerProvider: unique symbol;
