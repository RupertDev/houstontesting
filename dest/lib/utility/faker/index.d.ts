/**
 * houston/src/lib/utility/faker/index.ts
 * Creates fake data. Used for testing and factories
 */
import * as BaseFaker from 'faker/lib/index';
import { FakerProvider } from './type';
export declare class Faker extends BaseFaker {
    locale: string;
    /**
     * Creates a new new Faker instance
     *
     * @param {FakerProvider[]} [providers]
     */
    constructor(providers?: FakerProvider[]);
    protected registerProvider(provider: FakerProvider): void;
}
