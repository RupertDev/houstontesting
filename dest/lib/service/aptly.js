"use strict";
/**
 * houston/src/lib/service/aptly/package-repo.ts
 * Class for aptly package stuff
 */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const crypto = require("crypto");
const inversify_1 = require("inversify");
const agent = require("superagent");
const config_1 = require("../config");
const log_1 = require("../log");
/**
 * This is an array of architectures we will publish to if we don't have any
 * given in the configuration.
 *
 * @var {String[]}
 */
const DEFAULT_ARCHITECTURES = ['amd64'];
/**
 * This is the default distribution we will publish for if non is given
 *
 * @var {String}
 */
const DEFAULT_DISTRIBUTION = 'xenial';
/**
 * Creates a url from the strings given fixing weird undefined errors.
 *
 * @param {...String} args
 * @return {String}
 */
function createUrl(...args) {
    return args
        .join('/')
        .replace(/(?<!:)\/{2,}/, '/');
}
let Aptly = class Aptly {
    /**
     * Creates a new aptly package repository
     *
     * @param {Config} config
     */
    constructor(config, logger) {
        /**
         * The human readable name of the service.
         *
         * @var {String}
         */
        this.serviceName = 'elementary Package Repository';
        this.config = config;
        this.logger = logger;
    }
    /**
     * Takes a full path to file and uploads it to a package repository
     *
     * @async
     * @param {IPackage} pkg The package to upload
     * @param {IStage} [stage] The build stage the package is in
     * @param {String} [reference] A code-repository reference that this file was from
     * @return {IPackage} Package with an updated aptly id
     */
    uploadPackage(pkg, stage, reference) {
        return __awaiter(this, void 0, void 0, function* () {
            const rootUrl = this.config.get('service.aptly.url');
            const details = this.getAptlyDetails(pkg, stage);
            if (pkg.aptlyId == null) {
                // Some background here: We need this to be unique to each package for
                // Testing reasons (matching nock request urls), but we want it to be
                // Random enough that we will not get collisions. This does not need to be
                // Secure for any reasons.
                const fileHash = crypto
                    .createHash('md5')
                    .update(JSON.stringify(pkg))
                    .digest('hex');
                const fileName = `${fileHash}.${pkg.type}`;
                yield agent
                    .post(createUrl(rootUrl, 'files', fileName))
                    .attach('file', pkg.path, fileName);
                const aptlyFileName = yield agent
                    .post(createUrl(rootUrl, 'repos', details.prefix, 'file', fileName))
                    .then((data) => {
                    if (data.body.FailedFiles != null && data.body.FailedFiles.length > 0) {
                        throw new Error('Unable to add package to Aptly');
                    }
                    else {
                        // Aptly adds an ' added' string
                        return data.body.Report.Added[0].split(' ')[0];
                    }
                });
                // Aptly requires us to do _yet another_ API call to get the package ID :/
                pkg.aptlyId = yield agent
                    .get(createUrl(rootUrl, 'repos', details.prefix, 'packages'))
                    .query({ q: aptlyFileName })
                    .then((data) => {
                    if (data.body == null || data.body.length < 1) {
                        throw new Error('Unable to find uploaded Aptly package');
                    }
                    else {
                        return data.body[0];
                    }
                });
                yield agent.delete(createUrl(rootUrl, 'files', fileName));
            }
            yield agent
                .post(createUrl(rootUrl, 'repos', details.prefix, 'packages'))
                .send({ PackageRefs: [pkg.aptlyId] });
            return pkg;
        });
    }
    /**
     * Resolves all the details aptly needs to publish
     *
     * @param {IPackage} pkg
     * @param {IStage} stage
     * @return {IAptlyDetails}
     */
    getAptlyDetails(pkg, stage) {
        const resolveFn = (value) => {
            const defaultValue = {
                architectures: ((value || {}).architectures || DEFAULT_ARCHITECTURES),
                distribution: ((value || {}).distribution || pkg.distribution || DEFAULT_DISTRIBUTION),
                prefix: (value || {}).prefix
            };
            switch (typeof value) {
                case ('string'):
                    return Object.assign({}, defaultValue, { prefix: value });
                case ('function'):
                    return resolveFn(value(pkg));
                case ('object'):
                case ('undefined'):
                    return defaultValue;
                default:
                    throw new Error('Aptly settings returned unknown value.');
            }
        };
        return resolveFn(this.config.get(`service.aptly.${stage}`));
    }
};
Aptly = __decorate([
    inversify_1.injectable(),
    __param(0, inversify_1.inject(config_1.Config)), __param(1, inversify_1.inject(log_1.Logger)),
    __metadata("design:paramtypes", [config_1.Config, log_1.Logger])
], Aptly);
exports.Aptly = Aptly;
//# sourceMappingURL=aptly.js.map