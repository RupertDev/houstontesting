/**
 * houston/src/lib/service/index.ts
 * Entry file to all of the fun third party services we interact with
 */
import * as type from './type';
/**
 * Creates a new repository service given any URL
 * TODO: Add more repository services besides GitHub
 *
 * @param {string} url
 * @return {Repository}
 */
export declare function createCodeRepository(url: string): type.ICodeRepository;
export declare function isCodeRepository(value: any): value is type.ICodeRepository;
export declare function isPackageRepository(value: any): value is type.IPackageRepository;
export declare function isLogRepository(value: any): value is type.ILogRepository;
export { ICodeRepository, ILog, IPackage, IPackageRepository, IServiceIds } from './type';
export { Aptly } from './aptly';
export { GitHub } from './github';
export { GitLab } from './gitlab';
export declare const codeRepository: unique symbol;
export declare const packageRepository: unique symbol;
export declare const logRepository: unique symbol;
