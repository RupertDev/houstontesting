/**
 * houston/src/lib/service/gitlab.ts
 * Handles interaction with GitLab repositories.
 */
import * as type from './type';
export declare class GitLab implements type.ICodeRepository, type.IPackageRepository, type.ILogRepository {
    /**
     * tmpFolder
     * Folder to use as scratch space for cloning repos
     *
     * @var {string}
     */
    protected static tmpFolder: string;
    /**
     * The human readable name of the service.
     *
     * @var {String}
     */
    serviceName: string;
    /**
     * username
     * The GitLab username or organization.
     *
     * @var {string}
     */
    username: string;
    /**
     * repository
     * The GitLab user's repository name
     *
     * @var {string}
     */
    repository: string;
    /**
     * auth
     * Authentication to use when interacting
     *
     * @var {string}
     */
    auth?: string;
    /**
     * reference
     * The reference to branch or tag.
     *
     * @var {string}
     */
    reference: string;
    /**
     * Tries to get the file mime type by reading the first chunk of a file.
     * Required by GitLab API
     *
     * Code taken from examples of:
     * https://github.com/sindresorhus/file-type
     * https://github.com/sindresorhus/read-chunk/blob/master/index.js
     *
     * @async
     * @param {String} p Full file path
     * @return {String} The file mime type
     */
    protected static getFileType(p: string): Promise<string>;
    /**
     * Creates a new GitLab Repository
     *
     * @param {string} url - The full gitlab url
     */
    constructor(url: string);
    /**
     * url
     * Returns the Git URL for the repository
     *
     * @return {string}
     */
    /**
    * url
    * Sets the Git URL for the repository
    * NOTE: Auth code is case sensitive, so we can't lowercase the whole url
    *
    * @return {string}
    */
    url: string;
    /**
     * Returns the default RDNN value for this repository
     *
     * @return {string}
     */
    readonly rdnn: string;
    /**
     * clone
     * Clones the repository to a folder
     *
     * @async
     * @param {string} p - The path to clone to
     * @param {string} [reference] - The branch to clone
     * @return {void}
     */
    clone(p: string, reference?: string): Promise<void>;
    /**
     * references
     * Returns a list of references this repository has
     * TODO: Try to figure out a more optimized way
     *
     * @async
     * @return {string[]}
     */
    references(): Promise<string[]>;
    /**
     * Uploads an asset to a GitLab release.
     *
     * @async
     * @param {IPackage} pkg Package to upload
     * @param {IStage} stage
     * @param {string} [reference]
     * @return {IPackage}
     */
    uploadPackage(pkg: type.IPackage, stage: type.IStage, reference?: string): Promise<{
        gitlabId: any;
        type: string;
        path: string;
        architecture?: string;
        distribution?: string;
        name: string;
        description?: string;
        aptlyId?: string;
        githubId?: number;
    }>;
    /**
     * Uploads a log to GitLab as an issue with the 'AppCenter' label
     *
     * @async
     * @param {ILog} log
     * @param {IStage} stage
     * @param {string} [reference]
     * @return {ILog}
     */
    uploadLog(log: type.ILog, stage: type.IStage, reference?: string): Promise<type.ILog>;
}
