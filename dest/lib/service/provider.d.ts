/**
 * houston/src/lib/service/provider.ts
 * Registers all the fun third party services that require the IoC container
 */
import { ContainerModule } from 'inversify';
export declare const provider: ContainerModule;
