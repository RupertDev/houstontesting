"use strict";
/**
 * houston/src/lib/service/provider.ts
 * Registers all the fun third party services that require the IoC container
 */
Object.defineProperty(exports, "__esModule", { value: true });
const inversify_1 = require("inversify");
const index_1 = require("./index");
const aptly_1 = require("./aptly");
exports.provider = new inversify_1.ContainerModule((bind) => {
    // TODO: Bind GitHub classes
    bind(aptly_1.Aptly).toSelf();
    bind(index_1.packageRepository).to(aptly_1.Aptly);
});
//# sourceMappingURL=provider.js.map