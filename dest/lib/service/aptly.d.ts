/**
 * houston/src/lib/service/aptly/package-repo.ts
 * Class for aptly package stuff
 */
import { Config } from '../config';
import { Logger } from '../log';
import * as type from './type';
export interface IAptlyDetails {
    prefix?: string;
    distribution: string;
    architectures: string[];
}
export declare class Aptly implements type.IPackageRepository {
    /**
     * The human readable name of the service.
     *
     * @var {String}
     */
    serviceName: string;
    /**
     * The application configuration
     *
     * @var {Config}
     */
    protected config: Config;
    /**
     * A logger for the Aptly third party service
     *
     * @var {Logger}
     */
    protected logger: Logger;
    /**
     * Creates a new aptly package repository
     *
     * @param {Config} config
     */
    constructor(config: Config, logger: Logger);
    /**
     * Takes a full path to file and uploads it to a package repository
     *
     * @async
     * @param {IPackage} pkg The package to upload
     * @param {IStage} [stage] The build stage the package is in
     * @param {String} [reference] A code-repository reference that this file was from
     * @return {IPackage} Package with an updated aptly id
     */
    uploadPackage(pkg: type.IPackage, stage?: type.IStage, reference?: string): Promise<type.IPackage>;
    /**
     * Resolves all the details aptly needs to publish
     *
     * @param {IPackage} pkg
     * @param {IStage} stage
     * @return {IAptlyDetails}
     */
    protected getAptlyDetails(pkg: type.IPackage, stage: type.IStage): IAptlyDetails;
}
