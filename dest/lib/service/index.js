"use strict";
/**
 * houston/src/lib/service/index.ts
 * Entry file to all of the fun third party services we interact with
 */
Object.defineProperty(exports, "__esModule", { value: true });
const github_1 = require("./github");
const gitlab_1 = require("./gitlab");
/**
 * Creates a new repository service given any URL
 * TODO: Add more repository services besides GitHub
 *
 * @param {string} url
 * @return {Repository}
 */
function createCodeRepository(url) {
    if (url.search('gitlab') > 0) {
        return new gitlab_1.GitLab(url);
    }
    else {
        return new github_1.GitHub(url);
    }
}
exports.createCodeRepository = createCodeRepository;
// Typescript typeguard to ensure given value is an ICodeRepository
function isCodeRepository(value) {
    return (value != null && typeof value.clone === 'function');
}
exports.isCodeRepository = isCodeRepository;
// Typescript typeguard to ensure given value is an IPackageRepository
function isPackageRepository(value) {
    return (value != null && typeof value.uploadPackage === 'function');
}
exports.isPackageRepository = isPackageRepository;
// Typescript typeguard to ensure given value is an ILogRepository
function isLogRepository(value) {
    return (value != null && typeof value.uploadLog === 'function');
}
exports.isLogRepository = isLogRepository;
var aptly_1 = require("./aptly");
exports.Aptly = aptly_1.Aptly;
var github_2 = require("./github");
exports.GitHub = github_2.GitHub;
var gitlab_2 = require("./gitlab");
exports.GitLab = gitlab_2.GitLab;
exports.codeRepository = Symbol('codeRepository');
exports.packageRepository = Symbol('packageRepository');
exports.logRepository = Symbol('logRepository');
//# sourceMappingURL=index.js.map