/**
 * houston/src/lib/queue/providers/redis/queue.ts
 * Wraps `bull` package in types for use in our queue system.
 */
import * as BaseBull from 'bull';
import { Config } from '../../../config';
import * as type from '../../type';
import { Job } from './job';
export declare class Queue implements type.IQueue {
    /**
     * The bull instance we will be proxying to
     *
     * @var {Bull}
     */
    protected bull: BaseBull.Queue;
    /**
     * Creates a new queue with the given name
     *
     * @param {String} name
     */
    constructor(config: Config, name: string);
    send(data: any, opts: any): Promise<Job>;
    handle(fn: any): Promise<void>;
    pause(local: any): Promise<void>;
    resume(local: any): Promise<void>;
    empty(): Promise<void>;
    close(): Promise<void>;
    count(state?: any): Promise<number>;
    jobs(state: type.Status): Promise<Job[]>;
    onActive(fn: any): void;
    onProgress(fn: any): void;
    onFailed(fn: any): void;
    onCompleted(fn: any): void;
    protected bullJobs(state: type.Status): Promise<BaseBull.Job[]>;
}
