/**
 * houston/src/lib/queue/providers/redis/job.ts
 * Wraps `bull` package in types for use in our queue system.
 */
/// <reference types="node" />
import * as BaseBull from 'bull';
import { EventEmitter } from 'events';
import * as type from '../../type';
export declare class Job extends EventEmitter implements type.IJob {
    /**
     * The bull instance we will be proxying to
     *
     * @var {Bull}
     */
    protected bull: BaseBull.Job;
    /**
     * Creates a new queue with the given name
     *
     * @param {String} name
     */
    constructor(job: BaseBull.Job);
    status(): Promise<type.Status>;
    progress(amount: any): Promise<void>;
    remove(): Promise<void>;
}
