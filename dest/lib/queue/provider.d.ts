/**
 * houston/src/lib/queue/provider.ts
 * Sets up the needed providers for the Queue system
 */
import { ContainerModule } from 'inversify';
export declare const provider: ContainerModule;
