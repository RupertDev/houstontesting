/**
 * houston/src/lib/queue/index.ts
 * Exports all you need to get started with the queue system.
 */
export { Status } from './type';
export declare const Queue: unique symbol;
export declare const workerQueue: unique symbol;
