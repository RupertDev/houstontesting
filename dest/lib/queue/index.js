"use strict";
/**
 * houston/src/lib/queue/index.ts
 * Exports all you need to get started with the queue system.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.Queue = Symbol.for('IQueue'); // tslint:disable-line
exports.workerQueue = Symbol.for('workerQueue'); // tslint:disable-line
//# sourceMappingURL=index.js.map