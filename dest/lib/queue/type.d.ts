/**
 * houston/src/lib/queue/type.ts
 * Some typescript types for a queue system.
 */
export declare type Status = 'waiting' | 'active' | 'completed' | 'failed' | 'delayed';
export declare type HandleCallback = (job: IJob) => Promise<object>;
export declare type OnActiveCallback = (job: IJob) => void;
export declare type OnProgressCallback = (job: IJob, amount: number) => void;
export declare type OnFailedCallback = (job: IJob, error: Error) => void;
export declare type OnCompletedCallback = (job: IJob, result: object) => void;
export declare type IQueueConstructor = (name: string) => IQueue;
export interface IQueue {
    send(data: object, opts?: IJobOptions): Promise<IJob>;
    handle(fn: HandleCallback): any;
    pause(local: boolean): Promise<void>;
    resume(local: boolean): Promise<void>;
    empty(): Promise<void>;
    close(): Promise<void>;
    count(state?: Status): Promise<number>;
    jobs(state: Status): Promise<IJob[]>;
    onActive(fn: OnActiveCallback): any;
    onProgress(fn: OnProgressCallback): any;
    onFailed(fn: OnFailedCallback): any;
    onCompleted(fn: OnCompletedCallback): any;
}
export interface IJobOptions {
    priority?: number;
    delay?: number;
    attempts?: number;
    timeout?: number;
}
export interface IJob {
    status(): Promise<Status>;
    progress(amount: number): any;
    remove(): Promise<void>;
}
