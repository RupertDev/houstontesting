/**
 * houston/src/lib/app.ts
 * IOC container for houston. This is the entrypoint to anything and everything
 * sweat in life.
 */
import { Container, ContainerModule } from 'inversify';
import { Config } from './config';
/**
 * App
 * A houston IOC container
 */
export declare class App extends Container {
    /**
     * A list of all the providers to load in the application.
     *
     * @var {ContainerModule[]}
     */
    protected static providers: ContainerModule[];
    /**
     * Creates a new App
     *
     * @param {Config} config
     */
    constructor(config: Config);
    /**
     * Sets up all of the providers we have throughout the application.
     *
     * @return {void}
     */
    setupProviders(): void;
}
