"use strict";
/**
 * houston/src/lib/service/gitlab.e2e.ts
 * Tests the GitLab class.
 */
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs = require("fs-extra");
const path = require("path");
const uuid = require("uuid/v4");
const Log = require("../log");
const gitlab_1 = require("./gitlab");
const fs_1 = require("../../../test/utility/fs");
const http_1 = require("../../../test/utility/http");
let testingDir;
beforeAll(() => __awaiter(this, void 0, void 0, function* () {
    // I'm so sorry to whom is reading this next. My best guess is a race
    // Condition in fs-extra, uuid, or jest. This whole file will just hang
    // Forever without it. I'm so sorry.
    yield new Promise((resolve, reject) => {
        setTimeout(() => __awaiter(this, void 0, void 0, function* () {
            testingDir = yield fs_1.tmp('lib/service/gitlab');
            // Redirect tmp folder for testing because testing
            gitlab_1.GitLab.tmpFolder = testingDir;
            return resolve();
        }), 100);
    });
}));
afterAll(() => __awaiter(this, void 0, void 0, function* () {
    yield fs.remove(testingDir);
}));
test('can clone a repository', () => __awaiter(this, void 0, void 0, function* () {
    const repo = new gitlab_1.GitLab('https://gitlab.com/RupertDev/houstontesting');
    const folder = path.resolve(testingDir, uuid());
    yield fs.mkdirs(folder);
    yield repo.clone(folder);
    const stat = yield fs.stat(folder);
    expect(stat.isDirectory()).toBeTruthy();
}), 600000); // 10 minutes because of git clone
test('can clone a repository with tag', () => __awaiter(this, void 0, void 0, function* () {
    const repo = new gitlab_1.GitLab('https://gitlab.com/RupertDev/houstontesting');
    const folder = path.resolve(testingDir, uuid());
    yield fs.mkdirs(folder);
    yield repo.clone(folder, 'refs/tags/0.2.0');
    const stat = yield fs.stat(folder);
    expect(stat.isDirectory()).toBeTruthy();
    // tslint:disable-next-line non-literal-require
    const pkg = require(path.resolve(folder, 'package.json'));
    expect(pkg).toHaveProperty('version');
    expect(pkg.version).toEqual('0.1.8');
}), 600000); // 10 minutes because of git clone
test.skip('can clone a repository with a non-annotated tag (#511)', () => __awaiter(this, void 0, void 0, function* () {
    const repo = new gitlab_1.GitLab('https://gitlab.com/fluks-eos/gdice');
    const folder = path.resolve(testingDir, uuid());
    yield fs.mkdirs(folder);
    yield repo.clone(folder, 'refs/tags/v1.0.1');
    const stat = yield fs.stat(folder);
    expect(stat.isDirectory()).toBeTruthy();
}), 600000); // 10 minutes because of git clone
test('can list all references for a repository', () => __awaiter(this, void 0, void 0, function* () {
    const repo = new gitlab_1.GitLab('https://gitlab.com/RupertDev/houstontesting');
    const references = yield repo.references();
    expect(references).toContain('refs/heads/master');
    expect(references).toContain('refs/remotes/origin/v2'); // TODO: Future me: remove this
}), 600000); // 10 minutes because of git clone for references
test('can post assets to reference', () => __awaiter(this, void 0, void 0, function* () {
    const { done } = yield http_1.record('lib/service/gitlab/asset.json');
    const repo = new gitlab_1.GitLab('https://gitlab.com/btkostner/vocal');
    const pkg = {
        architecture: 'amd64',
        description: 'Vocal 3.2.6 Loki (amd64)',
        distribution: 'xenial',
        name: 'package.deb',
        path: path.resolve(__dirname, '../../../test/fixture/lib/service/gitlab/vocal.deb'),
        type: 'deb'
    };
    const newPkg = yield repo.uploadPackage(pkg, 'review', '3.2.6');
    expect(newPkg.gitlabId).toBe(6174740);
    yield done();
}));
test('can post an log', () => __awaiter(this, void 0, void 0, function* () {
    const { done } = yield http_1.record('lib/service/gitlab/log.json');
    const repo = new gitlab_1.GitLab('https://gitlab.com/btkostner/vocal');
    const log = {
        body: 'testy test test',
        level: Log.Level.ERROR,
        title: 'test'
    };
    const newLog = yield repo.uploadLog(log, 'review', '3.2.6');
    expect(newLog.gitlabId).toBe(326839748);
    yield done();
}));
//# sourceMappingURL=gitlab.e2e.js.map