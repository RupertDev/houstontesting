"use strict";
/**
 * houston/src/lib/service/github.ts
 * Handles interaction with GitHub repositories.
 */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
var GitHub_1;
"use strict";
const URL = require("url");
const fileType = require("file-type");
const fs = require("fs-extra");
const inversify_1 = require("inversify");
const Git = require("nodegit");
const os = require("os");
const path = require("path");
const agent = require("superagent");
const uuid = require("uuid/v4");
const rdnn_1 = require("../utility/rdnn");
let GitHub = GitHub_1 = class GitHub {
    /**
     * Creates a new GitHub Repository
     *
     * @param {string} url - The full github url
     */
    constructor(url) {
        /**
         * The human readable name of the service.
         *
         * @var {String}
         */
        this.serviceName = 'GitHub';
        /**
         * reference
         * The reference to branch or tag.
         *
         * @var {string}
         */
        this.reference = 'refs/heads/master';
        this.url = url;
    }
    /**
     * Tries to get the file mime type by reading the first chunk of a file.
     * Required by GitHub API
     *
     * Code taken from examples of:
     * https://github.com/sindresorhus/file-type
     * https://github.com/sindresorhus/read-chunk/blob/master/index.js
     *
     * @async
     * @param {String} p Full file path
     * @return {String} The file mime type
     */
    static getFileType(p) {
        return __awaiter(this, void 0, void 0, function* () {
            const buffer = yield new Promise((resolve, reject) => {
                fs.open(p, 'r', (openErr, fd) => {
                    if (openErr != null) {
                        return reject(openErr);
                    }
                    fs.read(fd, Buffer.alloc(4100), 0, 4100, 0, (readErr, bytesRead, buff) => {
                        fs.close(fd);
                        if (readErr != null) {
                            return reject(readErr);
                        }
                        if (bytesRead < 4100) {
                            return resolve(buff.slice(0, bytesRead));
                        }
                        else {
                            return resolve(buff);
                        }
                    });
                });
            });
            return fileType(buffer).mime;
        });
    }
    /**
     * url
     * Returns the Git URL for the repository
     *
     * @return {string}
     */
    get url() {
        if (this.auth != null) {
            return `https://x-access-token:${this.auth}@github.com/${this.username}/${this.repository}.git`;
        }
        return `https://github.com/${this.username}/${this.repository}.git`;
    }
    /**
     * url
     * Sets the Git URL for the repository
     * NOTE: Auth code is case sensitive, so we can't lowercase the whole url
     *
     * @return {string}
     */
    set url(p) {
        if (p.indexOf('github') === -1) {
            throw new Error('Given URL is not a GitHub repository');
        }
        // NOTE: This is A+ 10/10 string logic. Will not break ever.
        const httpPath = (p.startsWith('git@') ? p.replace('git@', 'https://') : p)
            .replace('://github.com:', '://github.com/')
            .replace(/\.git$/, '');
        const url = new URL.URL(httpPath);
        const [, username, repository] = url.pathname.split('/');
        this.username = username;
        this.repository = repository;
        this.auth = url.password || url.username || null;
    }
    /**
     * Returns the default RDNN value for this repository
     *
     * @return {string}
     */
    get rdnn() {
        return rdnn_1.sanitize(`com.github.${this.username}.${this.repository}`);
    }
    /**
     * clone
     * Clones the repository to a folder
     *
     * @async
     * @param {string} p - The path to clone to
     * @param {string} [reference] - The branch to clone
     * @return {void}
     */
    clone(p, reference = this.reference) {
        return __awaiter(this, void 0, void 0, function* () {
            const repo = yield Git.Clone(this.url, p);
            const ref = yield Git.Reference.lookup(repo, reference);
            yield repo.checkoutRef(ref);
            yield fs.remove(path.resolve(p, '.git'));
        });
    }
    /**
     * references
     * Returns a list of references this repository has
     * TODO: Try to figure out a more optimized way
     *
     * @async
     * @return {string[]}
     */
    references() {
        return __awaiter(this, void 0, void 0, function* () {
            const p = path.resolve(GitHub_1.tmpFolder, uuid());
            const repo = yield Git.Clone(this.url, p);
            const branches = yield repo.getReferenceNames(Git.Reference.TYPE.LISTALL);
            yield fs.remove(p);
            return branches;
        });
    }
    /**
     * Uploads an asset to a GitHub release.
     *
     * @async
     * @param {IPackage} pkg Package to upload
     * @param {IStage} stage
     * @param {string} [reference]
     * @return {IPackage}
     */
    uploadPackage(pkg, stage, reference) {
        return __awaiter(this, void 0, void 0, function* () {
            if (pkg.githubId != null) {
                return;
            }
            const url = `${this.username}/${this.repository}/releases/tags/${reference}`;
            const { body } = yield agent
                .get(`https://api.github.com/repos/${url}`)
                .set('accept', 'application/vnd.github.v3+json')
                .set('authorization', `Bearer ${this.auth}`);
            if (body.upload_url == null) {
                throw new Error('No Upload URL for GitHub release');
            }
            // TODO: Should we remove existing assets that would conflict?
            const mime = yield GitHub_1.getFileType(pkg.path);
            const stat = yield fs.stat(pkg.path);
            const file = yield fs.createReadStream(pkg.path);
            const res = yield new Promise((resolve, reject) => {
                let data = '';
                const req = agent
                    .post(body.upload_url.replace('{?name,label}', ''))
                    .set('content-type', mime)
                    .set('content-length', stat.size)
                    .set('authorization', `Bearer ${this.auth}`)
                    .query({ name: pkg.name })
                    .query((pkg.description != null) ? { label: pkg.description } : {})
                    .parse((response, fn) => {
                    response.on('data', (chunk) => { data += chunk; });
                    response.on('end', fn);
                })
                    .on('end', (err, response) => {
                    if (err != null) {
                        return reject(err);
                    }
                    try {
                        return resolve(JSON.parse(data));
                    }
                    catch (err) {
                        return reject(err);
                    }
                });
                file.pipe(req);
            });
            return Object.assign({}, pkg, { githubId: res.id });
        });
    }
    /**
     * Uploads a log to GitHub as an issue with the 'AppCenter' label
     *
     * @async
     * @param {ILog} log
     * @param {IStage} stage
     * @param {string} [reference]
     * @return {ILog}
     */
    uploadLog(log, stage, reference) {
        return __awaiter(this, void 0, void 0, function* () {
            if (log.githubId != null) {
                return;
            }
            const hasLabel = yield agent
                .get(`https://api.github.com/repos/${this.username}/${this.repository}/labels/AppCenter`)
                .set('authorization', `Bearer ${this.auth}`)
                .then(() => true)
                .catch(() => false);
            if (!hasLabel) {
                yield agent
                    .post(`https://api.github.com/repos/${this.username}/${this.repository}/labels`)
                    .set('authorization', `Bearer ${this.auth}`)
                    .send({
                    color: '4c158a',
                    description: 'Issues related to releasing on AppCenter',
                    name: 'AppCenter'
                });
            }
            const { body } = yield agent
                .post(`https://api.github.com/repos/${this.username}/${this.repository}/issues`)
                .set('authorization', `Bearer ${this.auth}`)
                .send({
                body: log.body,
                labels: ['AppCenter'],
                title: log.title
            });
            return Object.assign({}, log, { githubId: body.id });
        });
    }
};
/**
 * tmpFolder
 * Folder to use as scratch space for cloning repos
 *
 * @var {string}
 */
GitHub.tmpFolder = path.resolve(os.tmpdir(), 'houston');
GitHub = GitHub_1 = __decorate([
    inversify_1.injectable(),
    __metadata("design:paramtypes", [String])
], GitHub);
exports.GitHub = GitHub;
//# sourceMappingURL=github.js.map