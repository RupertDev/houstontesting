/**
 * houston/src/lib/log/logger.ts
 * A manager of logs and third party logging services.
 */
import { Config } from '../config';
import { Log } from './log';
import { Output, OutputConstructor } from './output';
/**
 * Log
 * A manager of logs and third party logging services
 */
export declare class Logger {
    /**
     * The configuration to use
     *
     * @var {Config}
     */
    protected config: Config;
    /**
     * A list of outputs that we will use when a log gets sent
     *
     * @var {Output[]}
     */
    protected outputs: Output[];
    /**
     * Creates a new logger
     *
     * @param {Config} config
     */
    constructor(config: Config);
    /**
     * Creates a new log
     *
     * @return {Log}
     */
    create(): Log;
    /**
     * Creates a new debug log
     *
     * @param {String} message
     * @return {Log}
     */
    debug(message: string): Log;
    /**
     * Creates a new info log
     *
     * @param {String} message
     * @return {Log}
     */
    info(message: string): Log;
    /**
     * Creates a new warn log
     *
     * @param {String} message
     * @return {Log}
     */
    warn(message: string): Log;
    /**
     * Creates a new error log
     *
     * @param {String} message
     * @return {Log}
     */
    error(message: string): Log;
    /**
     * Does things with a finished log.
     *
     * @param {Log} log
     */
    send(log: Log): void;
    /**
     * Sets up an output for the logger
     *
     * @param {OutputConstructor} outputter
     * @return {Logger}
     */
    protected setupOutput(outputter: OutputConstructor): this;
    /**
     * Given an array of outputters, we try to set them up
     *
     * @param {OutputConstructor[]} outputters
     * @return {Logger}
     */
    protected setupOutputs(outputters: OutputConstructor[]): this;
}
