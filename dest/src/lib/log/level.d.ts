/**
 * houston/src/lib/log/level.ts
 * Some log levels
 */
/**
 * The debug level of logs
 *
 * @var {Symbol}
 */
export declare const DEBUG: unique symbol;
/**
 * The info level of logs
 *
 * @var {Symbol}
 */
export declare const INFO: unique symbol;
/**
 * The warn level of logs
 *
 * @var {Symbol}
 */
export declare const WARN: unique symbol;
/**
 * The error level of logs
 *
 * @var {Symbol}
 */
export declare const ERROR: unique symbol;
/**
 * An enum representing all of the log levels
 *
 * @var {enum}
 */
export declare enum Level {
    DEBUG = 0,
    INFO = 1,
    WARN = 2,
    ERROR = 3
}
/**
 * Parses a string value for a level symbol
 *
 * @param {string} level
 * @return {Level}
 */
export declare function parseLevel(level: string): Level;
/**
 * Returns a string given a level symbol
 *
 * @param {Level} level
 * @return {string}
 */
export declare function levelString(level: Level): string;
/**
 * Returns a number index of severity for a level symbol
 *
 * @param {Level} level
 * @return {Number}
 */
export declare function levelIndex(level: Level): number;
