/**
 * houston/src/lib/log/services/sentry.ts
 * Handles logging errors to sentry
 */
import { Config } from '../../config';
import { Log } from '../log';
import { Output } from '../output';
export declare class Sentry extends Output {
    /**
     * The current application configuration
     *
     * @var {Config}
     */
    protected config: Config;
    /**
     * The sentry dns to use when reporting logs
     *
     * @var {String}
     */
    protected dns: string;
    /**
     * A raven instance for logging to sentry
     *
     * @var {Raven}
     */
    protected raven: any;
    /**
     * Checks if this output should be enabled
     *
     * @return {boolean}
     */
    static enabled(config: Config): boolean;
    /**
     * Creates a new Sentry output
     *
     * @param {Config} config
     */
    constructor(config: Config);
    /**
     * Sends error logs to sentry
     *
     * @param {Log} log
     * @return {void}
     */
    error(log: Log): void;
    /**
     * Transforms a log message to an error
     *
     * @param {Log} log
     *
     * @return {Error}
     */
    toError(log: Log): Error;
    /**
     * Sets up raven with common metadata and things.
     *
     * @return {Raven}
     */
    protected setup(): any;
}
