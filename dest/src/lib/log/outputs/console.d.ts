/**
 * houston/src/lib/log/services/console.ts
 * Outputs logs to the console
 */
import { Config } from '../../config';
import { Level } from '../level';
import { Log } from '../log';
import { Output } from '../output';
export declare class Console extends Output {
    /**
     * Configuration to use for console logs.
     *
     * @var {Config}
     */
    protected config: Config;
    /**
     * Checks if this output should be enabled
     *
     * @param {Config} config
     *
     * @return {boolean}
     */
    static enabled(config: Config): boolean;
    /**
     * Creates a new Sentry output
     *
     * @param {Config} config
     */
    constructor(config: Config);
    /**
     * Sends debug info to the console
     *
     * @param {Log} log
     * @return {void}
     */
    debug(log: Log): void;
    /**
     * Logs a message to the console
     *
     * @param {Log} log
     * @return {void}
     */
    info(log: Log): void;
    /**
     * Logs a warning log to the console
     *
     * @param {Log} log
     * @return {void}
     */
    warn(log: Log): void;
    /**
     * Logs an error to the console
     *
     * @param {Log} log
     * @return {void}
     */
    error(log: Log): void;
    /**
     * Checks if the configuration allows a given log level.
     *
     * @param {Level} level
     *
     * @return {Boolean}
     */
    protected allows(level: Level): boolean;
}
