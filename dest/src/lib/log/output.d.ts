/**
 * houston/src/lib/log/output.ts
 * An abstract class for sending logs somewhere.
 */
import { Config } from '../config';
import { Log } from './log';
/**
 * A generic abstract class for handling logs.
 */
export declare abstract class Output {
    /**
     * Checks if we should enable this output
     *
     * @param {Config} config
     *
     * @return {boolean}
     */
    static enabled(config: Config): boolean;
    /**
     * Creates a new logger output
     *
     * @param {Config} config
     */
    constructor(config: Config);
    /**
     * Does something with a debug log.
     *
     * @param {Log} log
     * @return {void}
     */
    debug(log: Log): void;
    /**
     * Does something with a info log.
     *
     * @param {Log} log
     * @return {void}
     */
    info(log: Log): void;
    /**
     * Does something with a warn log.
     *
     * @param {Log} log
     * @return {void}
     */
    warn(log: Log): void;
    /**
     * Does something with a error log.
     *
     * @param {Log} log
     * @return {void}
     */
    error(log: Log): void;
}
/**
 * An interface of the Output class as a constructor.
 * This is kinda pointless, but it keeps typescript happy when hinting.
 */
export interface OutputConstructor {
    new (config: Config): any;
    enabled(config: Config): boolean;
}
