"use strict";
/**
 * houston/src/lib/config/index.ts
 * Exports useful config things
 */
Object.defineProperty(exports, "__esModule", { value: true });
var config_1 = require("./config");
exports.Config = config_1.Config;
var loader_1 = require("./loader");
exports.getConfig = loader_1.getConfig;
//# sourceMappingURL=index.js.map