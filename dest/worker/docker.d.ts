/**
 * houston/src/worker/docker.ts
 * A helpful class for using docker.
 *
 * @exports {Class} Docker - A helpful class for using docker.
 */
/// <reference types="node" />
import * as Dockerode from 'dockerode';
import * as Stream from 'stream';
import { Config } from '../lib/config';
export declare class Docker {
    /**
     * log
     * The file to log to when running the container
     *
     * @var {string}
     */
    log?: string;
    /**
     * config
     * The configuration to use for connecting to docker
     *
     * @var {Config}
     */
    protected config: Config;
    /**
     * docker
     * The dockerode instance
     *
     * @var {Dockerode}
     */
    protected docker: Dockerode;
    /**
     * name
     * The docker image name to use
     *
     * @var {string}
     */
    protected name: string;
    /**
     * tag
     * The docker image tag
     */
    protected tag: string;
    /**
     * mounts
     * All of the directories that will be mounted on the container.
     * NOTE: Key is local folder, value is container folder.
     *
     * @var {object}
     */
    protected mounts: {};
    /**
     * Creates a new docker container class
     *
     * @param {Config} config - The configuration to use
     * @param {string} name - The docker image to use
     */
    constructor(config: Config, name: string);
    /**
     * options
     * All of the docker options that will get passed on running the container.
     *
     * @return {object}
     */
    readonly options: {
        Binds: string[];
    };
    /**
     * mount
     * Adds a mount point to the container
     *
     * @param {string} from - The local directory to attach
     * @param {string} to - The container directory to mount to
     * @return {Docker}
     */
    mount(from: string, to: string): this;
    /**
     * exists
     * Checks if the image currently exists.
     *
     * @async
     * @param {string} tag - Docker image tag to check for
     * @return {boolean}
     */
    exists(tag?: string): Promise<boolean>;
    /**
     * create
     * Creates a docker image from a directory of files.
     *
     * @async
     * @param {string} folder - The folder to create the image from
     * @return {void}
     */
    create(folder: string): Promise<void>;
    /**
     * run
     * Runs a container with the given command and mounts
     *
     * @param {string} cmd - Command to run
     * @param {object} [opts] - Additional options to pass to container
     * @return {number} - Container exit code
     */
    run(cmd: string, opts?: {}): Promise<number>;
    /**
     * setupLog
     * Creates / Clears the log file
     *
     * @async
     * @return {Stream|null}
     */
    protected setupLog(): Promise<Stream | null>;
}
