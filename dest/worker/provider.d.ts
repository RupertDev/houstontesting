/**
 * houston/src/worker/provider.ts
 * Provides the app with the main worker classes
 */
import { ContainerModule } from 'inversify';
export declare const provider: ContainerModule;
