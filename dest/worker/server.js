"use strict";
/**
 * houston/src/worker/server.ts
 * A server to listen for build requests
 *
 * @exports {Class} Server
 */
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
class Server {
    start() {
        return __awaiter(this, void 0, void 0, function* () {
            //
        });
    }
    stop() {
        return __awaiter(this, void 0, void 0, function* () {
            //
        });
    }
    branches(repository) {
        return __awaiter(this, void 0, void 0, function* () {
            //
        });
    }
    handleBuildRequest(storable) {
        return __awaiter(this, void 0, void 0, function* () {
            //
        });
    }
}
exports.Server = Server;
//# sourceMappingURL=server.js.map