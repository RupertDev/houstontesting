"use strict";
/**
 * houston/src/worker/preset/release.ts
 * Releases a package to all able endpoints.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const build_1 = require("./build");
const upload_1 = require("../task/upload");
function Release(app, repository, context) {
    const worker = build_1.Build(app, repository, context);
    worker.postTasks.push(upload_1.Upload);
    return worker;
}
exports.Release = Release;
//# sourceMappingURL=release.js.map