/**
 * houston/src/worker/preset/release.ts
 * Releases a package to all able endpoints.
 */
import { App } from '../../lib/app';
import { ICodeRepository } from '../../lib/service';
import * as type from '../type';
export declare function Release(app: App, repository: ICodeRepository, context: type.IContext): type.IWorker;
