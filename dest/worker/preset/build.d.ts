/**
 * houston/src/worker/preset/build.ts
 * Builds a package and edits contents for appcenter.
 */
import { App } from '../../lib/app';
import { ICodeRepository } from '../../lib/service';
import * as type from '../type';
export declare function Build(app: App, repository: ICodeRepository, context: type.IContext): type.IWorker;
