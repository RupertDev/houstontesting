/**
 * houston/src/worker/task/wrapperTask.ts
 * Runs a bunch of tasks in a row, collecting errors for later.
 */
import { Log } from '../log';
import { ITaskConstructor } from '../type';
import { Task } from './task';
export declare class WrapperTask extends Task {
    /**
     * The tasks to run
     *
     * @var {ITaskConstructor[]}
     */
    tasks: ITaskConstructor[];
    /**
     * All of the logs that where gathered
     *
     * @var {Log[]}
     */
    logs: Log[];
    /**
     * Returns all of the logs that are errors
     *
     * @return {Log[]}
     */
    protected readonly errorLogs: Log[];
    /**
     * Does logic.
     *
     * @async
     * @return {void}
     */
    run(): Promise<void>;
    /**
     * Runs all the tasks. This is out of the `run` method to allow easier
     * custom logic for WrapperTask runners
     *
     * @async
     * @return {void}
     */
    protected runTasks(): Promise<void>;
    /**
     * Catches an error thrown from one of the tasks
     *
     * @param {Error} e
     * @return {void}
     * @throws {Error}
     */
    protected catchError(e: Error): void;
}
