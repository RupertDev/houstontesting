/**
 * houston/src/worker/task/task.ts
 * Some worker logic.
 *
 * @exports {Class} Task
 */
import { ITask } from '../type';
import { Worker } from '../worker';
export declare class Task implements ITask {
    /**
     * The current running worker
     *
     * @var {Worker}
     */
    worker: Worker;
    /**
     * Creates a new Task
     *
     * @param {Worker} worker
     */
    constructor(worker: Worker);
    /**
     * Does logic.
     *
     * @async
     * @return {void}
     */
    run(): Promise<void>;
    /**
     * Adds a log/error to storage
     *
     * @param {Error} e
     * @return {Task}
     */
    report(e: Error): this;
}
