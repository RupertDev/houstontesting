/**
 * houston/src/worker/task/pack/deb.ts
 * Packages up an extracted deb file
 */
import { Docker } from '../../docker';
import { Task } from '../task';
export declare class PackDeb extends Task {
    /**
     * The directory we will pack to a deb file
     *
     * @return {string}
     */
    protected readonly path: string;
    /**
     * Runs liftoff
     *
     * @async
     * @return {void}
     */
    run(): Promise<void>;
    /**
     * Returns a docker instance to use for liftoff
     *
     * @async
     * @param {string} p - Folder to mount for building
     * @return {Docker}
     */
    protected docker(p: string): Promise<Docker>;
}
