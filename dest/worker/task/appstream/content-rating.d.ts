/**
 * houston/src/worker/task/appstream/content-rating.ts
 * Tests the OARS content rating in appstream files
 */
import { Task } from '../task';
export declare class AppstreamContentRating extends Task {
    /**
     * Path the appstream file should exist at
     *
     * @return {string}
     */
    readonly path: string;
    /**
     * All of the OARS content attributes to check for
     *
     * @var {string[]}
     */
    attributes: string[];
    /**
     * Runs all the appstream tests
     *
     * @async
     * @return {void}
     */
    run(): Promise<void>;
    /**
     * Checks if the given document has an OARS attribute
     *
     * @param {Object} $
     * @param {string} attribute
     * @return {Boolean}
     */
    protected hasAttribute($: any, attribute: any): boolean;
}
