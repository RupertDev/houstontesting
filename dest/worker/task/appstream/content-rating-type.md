# Missing AppStream OARS type

AppCenter could not find the `content_rating` `type` attribute.

Most projects should have an appstream file that includes:

```xml
<content_rating type="oars-1.1">
</content_rating>
```

For more information, please look at the
[Open Age Rating Service website](https://hughsie.github.io/oars/).
