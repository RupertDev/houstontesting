/**
 * houston/src/worker/task/appstream/summary.ts
 * Checks stuff about the appstream summary field
 */
import { Task } from '../task';
export declare class AppstreamSummary extends Task {
    /**
     * Path the appstream file should exist at
     *
     * @return {string}
     */
    readonly path: string;
    /**
     * Runs all the appstream tests
     *
     * @async
     * @return {void}
     */
    run(): Promise<void>;
}
