/**
 * houston/src/worker/task/appstream/stripe.ts
 * Adds stripe data to appstream file
 */
import { Task } from '../task';
export declare class AppstreamStripe extends Task {
    /**
     * Path the appstream file should exist at
     *
     * @return {string}
     */
    readonly path: string;
    /**
     * Runs all the appstream tests
     *
     * @async
     * @return {void}
     */
    run(): Promise<void>;
}
