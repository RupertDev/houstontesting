/**
 * houston/src/worker/task/appstream/index.ts
 * Runs a bunch of appstream tests
 * TODO: Only reports error and warn logs instead of all
 */
import { Log } from '../../log';
import { WrapperTask } from '../wrapperTask';
import { AppstreamDescription } from './description';
export declare class Appstream extends WrapperTask {
    /**
     * All of the fun tests we should run on the appstream file
     *
     * @var {Task[]}
     */
    readonly tasks: (typeof AppstreamDescription)[];
    /**
     * Path the appstream file should exist at
     *
     * @return {string}
     */
    readonly path: string;
    /**
     * All of the error logs that do not have a body
     *
     * @return {Log[]}
     */
    readonly errorPartials: Log[];
    /**
     * All of the warn logs that do not have a body
     *
     * @return {Log[]}
     */
    readonly warnPartials: Log[];
    /**
     * Runs all the appstream tests
     *
     * @async
     * @return {void}
     */
    run(): Promise<void>;
    /**
     * Concats any logs that don't have a body to a markdown template for easier
     * looking to the developer.
     *
     * @return {void}
     */
    protected concatLogs(): void;
}
