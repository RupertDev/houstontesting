/**
 * houston/src/worker/task/appstream/name.ts
 * Checks that a name field exists in the appstream file
 */
import { Task } from '../task';
export declare class AppstreamName extends Task {
    /**
     * Path the appstream file should exist at
     *
     * @return {string}
     */
    readonly path: string;
    /**
     * Runs all the appstream tests
     *
     * @async
     * @return {void}
     */
    run(): Promise<void>;
}
