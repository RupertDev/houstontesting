/**
 * houston/src/worker/task/appstream/id.ts
 * Tests the appstream ID matches correctly
 */
import { Task } from '../task';
export declare class AppstreamId extends Task {
    /**
     * Path the appstream file should exist at
     *
     * @return {string}
     */
    readonly path: string;
    /**
     * Runs all the appstream tests
     *
     * @async
     * @return {void}
     */
    run(): Promise<void>;
}
