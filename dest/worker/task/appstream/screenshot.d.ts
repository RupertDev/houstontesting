/**
 * houston/src/worker/task/appstream/screenshot.ts
 * Ensures the developer includes a screenshot
 */
import { Task } from '../task';
export declare class AppstreamScreenshot extends Task {
    /**
     * Path the appstream file should exist at
     *
     * @return {string}
     */
    readonly path: string;
    /**
     * Runs all the appstream tests
     *
     * @async
     * @return {void}
     */
    run(): Promise<void>;
    /**
     * Checks a screenshot tag in appstream file
     *
     * @param {Object} elem - Cheerio element
     * @return {void}
     */
    protected checkTag($: any, elem: any): void;
}
