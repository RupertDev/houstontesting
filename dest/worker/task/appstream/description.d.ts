/**
 * houston/src/worker/task/appstream/description.ts
 * Checks we have a description.
 */
import { Task } from '../task';
export declare class AppstreamDescription extends Task {
    /**
     * Path the appstream file should exist at
     *
     * @return {string}
     */
    readonly path: string;
    /**
     * Runs all the appstream tests
     *
     * @async
     * @return {void}
     */
    run(): Promise<void>;
}
