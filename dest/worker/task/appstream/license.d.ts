/**
 * houston/src/worker/task/appstream/license.ts
 * Checks that a project_license is in the appstream file
 */
import { Task } from '../task';
export declare class AppstreamLicense extends Task {
    /**
     * Path the appstream file should exist at
     *
     * @return {string}
     */
    readonly path: string;
    /**
     * Runs all the appstream tests
     *
     * @async
     * @return {void}
     */
    run(): Promise<void>;
}
