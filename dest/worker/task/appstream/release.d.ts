/**
 * houston/src/worker/task/appstream/release.ts
 * Checks and updates the appstream releases section
 */
import { Task } from '../task';
export declare class AppstreamRelease extends Task {
    /**
     * A list of valid tags for an appstream release
     *
     * @var {String[]}
     */
    protected static WHITELISTED_TAGS: string[];
    /**
     * The options needed for cheerio parsing
     *
     * @var {object}
     */
    protected static CHEERIO_OPTS: {
        useHtmlParser2: boolean;
        xmlMode: boolean;
    };
    /**
     * Path the appstream file should exist at
     *
     * @return {string}
     */
    readonly path: string;
    /**
     * Runs all the appstream tests
     *
     * @async
     * @return {void}
     */
    run(): Promise<void>;
    /**
     * Fills in the missing releases section
     *
     * @param {Object} $ - cheerio appstream document
     * @return {string} - The full appstream document after filling releases
     */
    protected fill($: any): any;
    /**
     * Parses a markdown changelog to find an urgency of the release
     *
     * @param {String} change
     * @return {String} - "low" "medium" "high" or "critical". "medium" is default
     */
    protected urgency(change: any): string;
    /**
     * Converts a markdown changelog to something appstream can deal with
     *
     * @param {String} change
     * @return {String}
     */
    protected html(change: any): any;
    /**
     * Sanitizes the html input to only allowed valid appstream tags
     *
     * @param {String} change
     * @return {String}
     */
    protected sanitize(change: any): any;
}
