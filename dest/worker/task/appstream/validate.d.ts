/**
 * houston/src/worker/task/appstream/validate.ts
 * Runs appstreamcli to validate appstream file
 */
import { Docker } from '../../docker';
import { Log } from '../../log';
import { Task } from '../task';
export declare class AppstreamValidate extends Task {
    /**
     * Location of the appstream cli log
     *
     * @return {string}
     */
    protected readonly logPath: string;
    /**
     * Path to folder containing the appstream file
     *
     * @return {string}
     */
    readonly path: string;
    /**
     * Runs appstream validate with docker
     *
     * @async
     * @return {void}
     */
    run(): Promise<void>;
    /**
     * Formats the docker log to something we can pass to the user
     *
     * @async
     * @return {Log}
     */
    protected log(): Promise<Log>;
    /**
     * Returns a docker instance to use for liftoff
     *
     * @async
     * @return {Docker}
     */
    protected docker(): Promise<Docker>;
}
