/**
 * houston/src/worker/task/extract/deb.ts
 * Extracts a deb package so we can test files
 */
import { Docker } from '../../docker';
import { Task } from '../task';
export declare class ExtractDeb extends Task {
    /**
     * The directory we will extract the deb file to
     *
     * @return {string}
     */
    protected readonly path: string;
    /**
     * Runs liftoff
     *
     * @async
     * @return {void}
     */
    run(): Promise<void>;
    /**
     * Ensures the extract path is created before we run docker
     *
     * @async
     * @return {void}
     */
    protected setup(): Promise<void>;
    /**
     * Returns a docker instance to use for liftoff
     *
     * @async
     * @param {string} p - Folder to mount for building
     * @return {Docker}
     */
    protected docker(p: string): Promise<Docker>;
}
