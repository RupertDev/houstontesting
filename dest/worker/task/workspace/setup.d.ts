/**
 * houston/src/worker/task/workspace/setup.ts
 * Fills the workspace with files from git
 */
import { Task } from '../task';
export declare class WorkspaceSetup extends Task {
    /**
     * These are all possible types of outputs we can have. They are used for
     * generating possible reference names.
     *
     * @var {String[]}
     */
    protected possiblePackageTypes: string[];
    protected possibleArchitectures: string[];
    protected possibleDistributions: string[];
    /**
     * Given two lists of strings we can find the first most common string.
     *
     * @param {String[]} references
     * @param {String[]} search - All of the reference parts we are looking for
     * @return {String[]}
     */
    protected static filterRefs(references: any, search: any): string[];
    /**
     * Fills the workspace by merging the release and package branches of a repo.
     *
     * @async
     * @return {void}
     */
    run(): Promise<void>;
    /**
     * Using all the possible combinations and repositories references we have,
     * this will generate a list of each package system, architecture, and
     * distribution we should build for.
     *
     * @async
     * @return {IBuildConfiguration[]}
     */
    private possibleBuilds;
    /**
     * Returns a list of branches to use for making the build.
     *
     * @async
     * @param {IBuildConfiguration} build
     * @return {String[]}
     */
    private branches;
}
