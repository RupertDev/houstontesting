"use strict";
/**
 * houston/src/worker/task/workspace/setup.ts
 * Fills the workspace with files from git
 */
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs = require("fs-extra");
const path = require("path");
const task_1 = require("../task");
class WorkspaceSetup extends task_1.Task {
    constructor() {
        super(...arguments);
        /**
         * These are all possible types of outputs we can have. They are used for
         * generating possible reference names.
         *
         * @var {String[]}
         */
        this.possiblePackageTypes = ['deb'];
        this.possibleArchitectures = ['amd64'];
        this.possibleDistributions = ['loki', 'juno'];
    }
    /**
     * Given two lists of strings we can find the first most common string.
     *
     * @param {String[]} references
     * @param {String[]} search - All of the reference parts we are looking for
     * @return {String[]}
     */
    static filterRefs(references, search) {
        // Gets the last part of a git reference "refs/origin/master" -> "master"
        const shortReferences = references
            .map((ref) => ref.split('/').reverse()[0]);
        return search
            .map((ref) => shortReferences.findIndex((short) => (short === ref)))
            .filter((ref) => (ref !== -1))
            .map((i) => references[i]);
    }
    /**
     * Fills the workspace by merging the release and package branches of a repo.
     *
     * @async
     * @return {void}
     */
    run() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.worker.emitAsync(`task:WorkspaceSetup:start`);
            const builds = yield this.possibleBuilds();
            if (builds.length === 0) {
                builds.push({
                    architecture: 'amd64',
                    distribution: 'loki',
                    packageType: 'deb'
                });
            }
            for (const build of builds) {
                const worker = yield this.worker.emitAsyncChain(`task:WorkspaceSetup:fork`, yield this.worker.fork({
                    architecture: build.architecture,
                    distribution: build.distribution,
                    package: { type: build.packageType }
                }));
                yield fs.ensureDir(worker.workspace);
                const branches = yield this.branches(build);
                // Step 1: Download all the needed branches
                for (let i = 0; i < branches.length; i++) {
                    // TODO: Maybe go and slugify the branch for easier debugging of folders
                    const gitFolder = path.resolve(worker.workspace, 'repository', `${i}`);
                    yield worker.repository.clone(gitFolder, branches[i]);
                }
                // Step 2: Merge the downloaded branches to form a single folder
                for (let i = 0; i < branches.length; i++) {
                    const from = path.resolve(worker.workspace, 'repository', `${i}`);
                    const to = path.resolve(worker.workspace, 'clean');
                    yield fs.copy(from, to, { overwrite: true });
                }
                // Step 3: Copy pasta to the dirty directory
                const clean = path.resolve(worker.workspace, 'clean');
                const dirty = path.resolve(worker.workspace, 'dirty');
                yield fs.ensureDir(clean);
                yield fs.ensureDir(dirty);
                yield fs.copy(clean, dirty);
            }
            // Step 4: Profit
            yield this.worker.emitAsync(`task:WorkspaceSetup:end`);
        });
    }
    /**
     * Using all the possible combinations and repositories references we have,
     * this will generate a list of each package system, architecture, and
     * distribution we should build for.
     *
     * @async
     * @return {IBuildConfiguration[]}
     */
    possibleBuilds() {
        return __awaiter(this, void 0, void 0, function* () {
            const repositoryReferences = yield this.worker.repository.references();
            const buildConfigurations = [];
            for (const packageType of this.possiblePackageTypes) {
                for (const architecture of this.possibleArchitectures) {
                    for (const distribution of this.possibleDistributions) {
                        const possibleReferences = [
                            `${distribution}`,
                            `${packageType}-packaging`,
                            `${packageType}-packaging-${distribution}`
                        ];
                        const matchingRefs = WorkspaceSetup.filterRefs(repositoryReferences, possibleReferences);
                        if (matchingRefs.length !== 0) {
                            buildConfigurations.push({
                                architecture,
                                distribution,
                                packageType
                            });
                        }
                    }
                }
            }
            return buildConfigurations;
        });
    }
    /**
     * Returns a list of branches to use for making the build.
     *
     * @async
     * @param {IBuildConfiguration} build
     * @return {String[]}
     */
    branches(build) {
        return __awaiter(this, void 0, void 0, function* () {
            const repositoryReferences = yield this.worker.repository.references();
            const mergableReferences = [
                `${build.distribution}`,
                `${build.packageType}-packaging`,
                `${build.packageType}-packaging-${build.distribution}`
            ];
            if (this.worker.context.references[0] != null) {
                const shortBranch = this.worker.context.references[0].split('/').reverse()[0];
                mergableReferences.push(`${build.packageType}-packaging-${build.distribution}-${shortBranch}`);
            }
            const packageReferences = WorkspaceSetup.filterRefs(repositoryReferences, mergableReferences);
            // Returns a unique array. No dups.
            return [...new Set([...this.worker.context.references, ...packageReferences])];
        });
    }
}
exports.WorkspaceSetup = WorkspaceSetup;
//# sourceMappingURL=setup.js.map