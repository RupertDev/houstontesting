/**
 * houston/src/worker/task/debian/control.ts
 * Updates, lints, and validates the Debian control file.
 */
import { Log } from '../../log';
import { Task } from '../task';
import { Parser } from './controlParser';
export declare class DebianControl extends Task {
    /**
     * File location for the debian control file
     *
     * @var {string}
     */
    static path: string;
    /**
     * The parser to use when doing stuff to the debian control file
     *
     * @var {Parser}
     */
    parser: Parser;
    /**
     * Returns the full path for the debian control file and the current test.
     *
     * @return {String}
     */
    protected readonly path: string;
    /**
     * Checks the Debian control file for errors
     *
     * @async
     * @return {void}
     */
    run(): Promise<void>;
    /**
     * Fills in missing data.
     *
     * @async'
     * @param {Object} data
     * @return {void}
     */
    protected fill(data: object): void;
    /**
     * Lints an object representation of the Debian control file.
     *
     * @async
     * @param {Object} data
     * @return {Log[]}
     */
    protected lint(data: object): Log[];
    /**
     * Inserts value into object it it does not yet exist
     *
     * @param {Object} data
     * @param {String} key
     * @param {String|Number} value
     * @return {void}
     */
    protected deepFill(data: object, key: string, value: string | number): void;
    /**
     * Asserts a deep value in the debian control file
     *
     * @param {Log[]} logs
     * @param {Object} data
     * @param {String} key
     * @param {String|Number|RegExp|Function|null} value
     * @param {String} [error]
     * @return {void}
     */
    protected deepAssert(logs: Log[], data: object, key: string, value: any, error?: string): void;
}
