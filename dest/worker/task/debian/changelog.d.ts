/**
 * houston/src/worker/task/debian/changelog.ts
 * Updates, lints, and validates the Debian changelog file.
 */
import { IChange, IContext } from '../../type';
import { Task } from '../task';
export declare class DebianChangelog extends Task {
    /**
     * File location for the debian changelog file
     *
     * @var {string}
     */
    static path: string;
    /**
     * File location for the debian changelog template
     *
     * @var {string}
     */
    static templatePath: string;
    /**
     * Returns the string templated version of the changelog
     *
     * @async
     * @param {IContext} context
     * @return {string}
     */
    static template(context: IContext): Promise<string>;
    /**
     * Recursivly gets a list of changes for each changelog item
     *
     * @async
     * @param {Object[]} changelogs
     * @return {Array[]}
     */
    protected static getChanges(changelogs?: any[]): Promise<string[][]>;
    /**
     * Parses a markdown string to find a list of changes
     *
     * @param {string} changes
     * @return {string[]}
     */
    protected static parseMarkdown(changes: any): Promise<string[]>;
    /**
     * Returns the full path for the debian changelog file and the current test.
     *
     * @return {String}
     */
    protected readonly path: string;
    /**
     * Checks the Debian control file for errors
     *
     * @async
     * @return {void}
     */
    run(): Promise<void>;
    /**
     * Fills the changelog file with all known changes
     * TODO: Convert markdown changes to a list
     *
     * @return {void}
     */
    fill(): Promise<void>;
    /**
     * Returns a blank change we will insert into the changelog
     *
     * @return {Object}
     */
    protected noopChange(): IChange;
}
