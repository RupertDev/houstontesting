/**
 * houston/src/worker/task/debian/controlParser.ts
 * Reads and writes a Debian control file. I whould highly recommend reading the
 * Debian documentation about the Control file before editing too much of this
 * file.
 * @see https://www.debian.org/doc/debian-policy/ch-controlfields.html
 */
export declare class Parser {
    /**
     * The order that debian control file properties should be in
     *
     * @var {string[]}
     */
    protected static order: string[];
    /**
     * The location to the file.
     *
     * @var {string}
     */
    readonly file: string;
    /**
     * Returns the type of line that is being parsed.
     *
     * @param {string} data - The raw string data
     * @param {number} line - The line to check type
     * @return {LineType}
     */
    private static readLineType;
    /**
     * Given some JavaScript data, we tell you what type of line to write.
     *
     * @param {string|string[]} data - The data to check
     * @return {LineType}
     */
    private static writeLineType;
    /**
     * Sorts an object to an array. It's pretty hacky, but we need to keep order.
     *
     * @param {Object} data
     * @return {array}
     */
    private static sortProperties;
    /**
     * Pads the left side of a string by a length
     *
     * @param {string} str
     * @param {Number} len
     * @return {string}
     */
    private static leftPad;
    /**
     * Creates a new Parser class
     *
     * @param {string} file - The location to the file.
     */
    constructor(file: string);
    /**
     * Reads the file and parses it to easy to understand javascript.
     *
     * @async
     * @return {object}
     */
    read(): Promise<object>;
    /**
     * Reads the file and parses it to easy to understand javascript.
     *
     * @async
     * @param {object} data - The data to write to file
     * @return {string}
     */
    write(data: object): Promise<string>;
}
