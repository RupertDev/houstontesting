/**
 * houston/src/worker/task/build/deb.ts
 * Builds a debian package
 */
import { Docker } from '../../docker';
import { Log } from '../../log';
import { IPackage } from '../../type';
import { Task } from '../task';
export declare class BuildDeb extends Task {
    /**
     * Returns the path of the liftoff cache
     *
     * @return {string}
     */
    protected static readonly cachePath: string;
    /**
     * Location of the liftoff log
     *
     * @return {string}
     */
    protected readonly logPath: string;
    /**
     * Location of the directory to build
     *
     * @return {string}
     */
    protected readonly path: string;
    /**
     * Returns the liftoff distribution to use.
     * NOTE: Because liftoff does not know about elementary distros, we map
     * them to the Ubuntu equivalents
     *
     * @return {string}
     */
    protected readonly distribution: string;
    /**
     * Runs liftoff
     *
     * @async
     * @return {void}
     */
    run(): Promise<void>;
    /**
     * Ensures the build directory is ready for docker
     *
     * @async
     * @return {void}
     */
    protected setup(): Promise<void>;
    /**
     * Returns the first known good package path. Used for when projects make
     * more than one package. We need to run tests on the main one.
     * TODO: Do tests on all the packages made
     *
     * @async
     * @return {IPackage|null}
     */
    protected package(): Promise<IPackage | null>;
    /**
     * Removes the messy build directory after copying the package to workspace
     *
     * @async
     * @return {void}
     */
    protected teardown(): Promise<void>;
    /**
     * Formats a liftoff error
     *
     * @async
     * @return {Log}
     */
    protected log(): Promise<Log>;
    /**
     * Returns a docker instance to use for liftoff
     *
     * @async
     * @return {Docker}
     */
    protected docker(): Promise<Docker>;
}
