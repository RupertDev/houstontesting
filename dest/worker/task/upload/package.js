"use strict";
/**
 * houston/src/worker/task/upload/package.ts
 * Responsible for uploading all the end results to third party services
 */
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const path = require("path");
const log_1 = require("../../../lib/log");
const log_2 = require("../../log");
const task_1 = require("../task");
const service_1 = require("../../../lib/service");
class UploadPackage extends task_1.Task {
    constructor() {
        super(...arguments);
        /**
         * A list of collected errors we get from third party services.
         *
         * @var {Object[]}
         */
        this.errors = [];
    }
    /**
     * Uploads all of the packages to third party services
     *
     * @async
     * @return {void}
     */
    run() {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.worker.context.package == null || this.worker.context.package.path == null) {
                return;
            }
            let pkg = this.worker.context.package;
            const ref = this.worker.context.references[this.worker.context.references.length - 1];
            pkg.name = `${this.worker.context.nameHuman} ${this.worker.context.version}`;
            pkg.description = [
                this.worker.context.nameDomain,
                this.worker.context.architecture,
                this.worker.context.distribution,
                this.worker.context.version
            ].join(' ');
            pkg = yield this.uploadToCodeRepository(pkg, ref);
            pkg = yield this.uploadToPackageRepositories(pkg, ref);
            this.worker.context.package = pkg;
            if (this.errors.length !== 0) {
                this.reportErrors();
            }
        });
    }
    /**
     * Uploads package to the origin code repository if it's also a package repo
     *
     * @async
     * @param {IPackage} pkg
     * @param {string} ref
     * @return {IPackage}
     */
    uploadToCodeRepository(pkg, ref) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!service_1.isPackageRepository(this.worker.repository)) {
                return;
            }
            try {
                pkg = yield this.worker.repository.uploadPackage(pkg, 'review', ref);
            }
            catch (error) {
                this.errors.push({ error, service: this.worker.repository.serviceName });
            }
            return pkg;
        });
    }
    /**
     * Uploads package to all known about package repositories
     *
     * @async
     * @param {IPackage} pkg
     * @param {string} ref
     * @return {IPackage}
     */
    uploadToPackageRepositories(pkg, ref) {
        return __awaiter(this, void 0, void 0, function* () {
            // Prevents error when no packageRepository is not bound in tests or something
            if (!this.worker.app.isBound(service_1.packageRepository)) {
                return;
            }
            const packageRepositories = this.worker.app.getAll(service_1.packageRepository);
            for (const repo of packageRepositories) {
                try {
                    pkg = yield repo.uploadPackage(pkg, 'review', ref);
                }
                catch (error) {
                    this.errors.push({ error, service: repo.serviceName });
                }
            }
            return pkg;
        });
    }
    /**
     * Concats all the errors we have and puts them to a nice markdown log.
     *
     * @throws {Log}
     */
    reportErrors() {
        const logger = this.worker.app.get(log_1.Logger);
        this.errors.map((e) => logger.error('Error uploading package').setError(e.error));
        if (this.errors.length !== 0) {
            const logPath = path.resolve(__dirname, 'package.md');
            throw log_2.Log.template(log_2.Log.Level.ERROR, logPath, {
                services: this.errors.map((e) => e.service)
            });
        }
    }
}
exports.UploadPackage = UploadPackage;
//# sourceMappingURL=package.js.map