"use strict";
/**
 * houston/src/worker/task/upload/index.ts
 * Responsible for uploading all the end results to third party services
 */
Object.defineProperty(exports, "__esModule", { value: true });
const wrapperTask_1 = require("../wrapperTask");
const log_1 = require("./log");
const package_1 = require("./package");
class Upload extends wrapperTask_1.WrapperTask {
    /**
     * All of the upload tasks we should run
     *
     * @var {Task[]}
     */
    get tasks() {
        return [
            package_1.UploadPackage,
            log_1.UploadLog
        ];
    }
}
exports.Upload = Upload;
//# sourceMappingURL=index.js.map