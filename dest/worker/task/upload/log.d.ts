/**
 * houston/src/worker/task/upload/log.ts
 * Uploads end error logs to third party services
 */
import { ILog } from '../../type';
import { Task } from '../task';
export declare class UploadLog extends Task {
    /**
     * A list of collected errors we get from third party services.
     *
     * @var {Object[]}
     */
    protected errors: {
        error: Error;
        service: string;
    }[];
    /**
     * Uploads all of the logs to third party services
     *
     * @async
     * @return {void}
     */
    run(): Promise<void>;
    /**
     * Uploads logs to the origin code repository if it's also a log repo
     *
     * @async
     * @param {ILog[]} logs
     * @param {string} ref
     * @return {ILog[]}
     */
    protected uploadToCodeRepository(logs: any, ref: any): Promise<ILog[]>;
    /**
     * Concats all the errors we have and puts them to a nice markdown log.
     *
     * @throws {Log}
     */
    protected reportErrors(): void;
}
