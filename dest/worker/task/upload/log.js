"use strict";
/**
 * houston/src/worker/task/upload/log.ts
 * Uploads end error logs to third party services
 */
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const path = require("path");
const log_1 = require("../../../lib/log");
const service_1 = require("../../../lib/service");
const log_2 = require("../../log");
const task_1 = require("../task");
class UploadLog extends task_1.Task {
    constructor() {
        super(...arguments);
        /**
         * A list of collected errors we get from third party services.
         *
         * @var {Object[]}
         */
        this.errors = [];
    }
    /**
     * Uploads all of the logs to third party services
     *
     * @async
     * @return {void}
     */
    run() {
        return __awaiter(this, void 0, void 0, function* () {
            let logs = this.worker.result.logs;
            const ref = this.worker.context.references[this.worker.context.references.length - 1];
            logs = yield this.uploadToCodeRepository(logs, ref);
            this.worker.context.logs = logs;
            if (this.errors.length !== 0) {
                this.reportErrors();
            }
        });
    }
    /**
     * Uploads logs to the origin code repository if it's also a log repo
     *
     * @async
     * @param {ILog[]} logs
     * @param {string} ref
     * @return {ILog[]}
     */
    uploadToCodeRepository(logs, ref) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!service_1.isLogRepository(this.worker.repository)) {
                return;
            }
            const newLogs = [];
            try {
                for (const log of logs) {
                    newLogs.push(yield this.worker.repository.uploadLog(log, 'review', ref));
                }
            }
            catch (error) {
                this.errors.push({ error, service: this.worker.repository.serviceName });
            }
            return newLogs;
        });
    }
    /**
     * Concats all the errors we have and puts them to a nice markdown log.
     *
     * @throws {Log}
     */
    reportErrors() {
        const logger = this.worker.app.get(log_1.Logger);
        this.errors.map((e) => logger.error('Error uploading logs').setError(e.error));
        if (this.errors.length !== 0) {
            const logPath = path.resolve(__dirname, 'log.md');
            throw log_2.Log.template(log_2.Log.Level.ERROR, logPath, {
                services: this.errors.map((e) => e.service)
            });
        }
    }
}
exports.UploadLog = UploadLog;
//# sourceMappingURL=log.js.map