/**
 * houston/src/worker/task/upload/package.ts
 * Responsible for uploading all the end results to third party services
 */
import { Task } from '../task';
import { IPackage } from '../../../lib/service';
export declare class UploadPackage extends Task {
    /**
     * A list of collected errors we get from third party services.
     *
     * @var {Object[]}
     */
    protected errors: {
        error: Error;
        service: string;
    }[];
    /**
     * Uploads all of the packages to third party services
     *
     * @async
     * @return {void}
     */
    run(): Promise<void>;
    /**
     * Uploads package to the origin code repository if it's also a package repo
     *
     * @async
     * @param {IPackage} pkg
     * @param {string} ref
     * @return {IPackage}
     */
    protected uploadToCodeRepository(pkg: any, ref: any): Promise<IPackage>;
    /**
     * Uploads package to all known about package repositories
     *
     * @async
     * @param {IPackage} pkg
     * @param {string} ref
     * @return {IPackage}
     */
    protected uploadToPackageRepositories(pkg: any, ref: any): Promise<IPackage>;
    /**
     * Concats all the errors we have and puts them to a nice markdown log.
     *
     * @throws {Log}
     */
    protected reportErrors(): void;
}
