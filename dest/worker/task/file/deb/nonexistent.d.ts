/**
 * houston/src/worker/task/file/deb/binary.ts
 * Tests debian packages for needed binary file
 */
import { Task } from '../../task';
export declare class FileDebNonexistent extends Task {
    /**
     * Folder where non-correctly installed files will end up in the Debian package
     *
     * @return {string}
     */
    protected readonly path: string;
    /**
     * Glob for non-correctly installed files
     *
     * @return {string}
     */
    protected readonly files: string;
    /**
     * Checks no files are incorrectly placed in the deb package
     *
     * @async
     * @return {void}
     */
    run(): Promise<void>;
}
