/**
 * houston/src/worker/task/file/deb/binary.ts
 * Tests debian packages for needed binary file
 */
import { Task } from '../../task';
export declare class FileDebBinary extends Task {
    /**
     * Location of the directory to build
     *
     * @return {string}
     */
    protected readonly path: string;
    /**
     * Runs liftoff
     *
     * @async
     * @return {void}
     */
    run(): Promise<void>;
    /**
     * Returns a list of useful files in the package. Filters out custom files
     *
     * @async
     * @return {string[]}
     */
    protected files(): Promise<string[]>;
}
