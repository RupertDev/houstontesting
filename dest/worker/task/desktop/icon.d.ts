/**
 * houston/src/worker/task/appstream/icon.ts
 * Checks that a icon field is matches app name in the desktop file
 */
import { Task } from '../task';
export declare class DesktopIcon extends Task {
    /**
     * Path the desktop file should exist at
     *
     * @return {string}
     */
    readonly path: string;
    /**
     * Checks Icon field in desktop file
     *
     * @async
     * @return {void}
     */
    run(): Promise<void>;
}
