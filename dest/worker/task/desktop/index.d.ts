/**
 * houston/src/worker/task/desktop/index.ts
 * Runs a bunch of tests around the desktop file
 */
import { WrapperTask } from '../wrapperTask';
import { DesktopExec } from './exec';
export declare class Desktop extends WrapperTask {
    /**
     * All of the fun tests we should run on the desktop file
     *
     * @var {Task[]}
     */
    readonly tasks: (typeof DesktopExec)[];
    /**
     * Path the desktop file should exist at
     *
     * @return {string}
     */
    readonly path: string;
    /**
     * Runs all the desktop tests
     *
     * @async
     * @return {void}
     */
    run(): Promise<void>;
}
