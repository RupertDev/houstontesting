/**
 * houston/src/worker/task/appstream/validate.ts
 * Runs desktop files through the `desktop-file-validate` command
 */
import { Docker } from '../../docker';
import { Log } from '../../log';
import { Task } from '../task';
export declare class DesktopValidate extends Task {
    /**
     * Path to folder containing the desktop files
     *
     * @return {string}
     */
    readonly path: string;
    /**
     * Runs appstream validate with docker
     *
     * @async
     * @return {void}
     */
    run(): Promise<void>;
    /**
     * Location of the desktop log file for the given test file
     *
     * @return {string}
     */
    protected logPath(file: string): string;
    /**
     * Formats the docker log to something we can pass to the user
     *
     * @async
     * @param {string[]} files
     * @return {Log}
     */
    protected log(files: string[]): Promise<Log>;
    /**
     * Returns a docker instance to use for liftoff
     *
     * @async
     * @param {string} file
     * @return {Docker}
     */
    protected docker(file: string): Promise<Docker>;
}
