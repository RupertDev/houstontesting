/**
 * houston/src/worker/task/appstream/exec.ts
 * Checks that a exec field starts with app name in the desktop file
 */
import { Task } from '../task';
export declare class DesktopExec extends Task {
    /**
     * Path the desktop file should exist at
     *
     * @return {string}
     */
    readonly path: string;
    /**
     * Checks Exec field in desktop file
     *
     * @async
     * @return {void}
     */
    run(): Promise<void>;
}
