"use strict";
/**
 * houston/src/worker/provider.ts
 * Provides the app with the main worker classes
 */
Object.defineProperty(exports, "__esModule", { value: true });
const inversify_1 = require("inversify");
const index_1 = require("./index");
exports.provider = new inversify_1.ContainerModule((bind) => {
    bind(index_1.Server).toConstructor(index_1.Server);
    bind(index_1.Worker).toConstructor(index_1.Worker);
});
//# sourceMappingURL=provider.js.map