/**
 * houston/src/worker/worker.ts
 * The master class for repository processing.
 *
 * @exports {Class} Worker - A processing class
 */
import { App } from '../lib/app';
import { Config } from '../lib/config';
import { ICodeRepository } from '../lib/service';
import { EventEmitter } from '../lib/utility/eventemitter';
import * as type from './type';
export declare class Worker extends EventEmitter implements type.IWorker {
    /**
     * app
     * The base App container. Used for roles that need access to something.
     *
     * @var {App}
     */
    app: App;
    /**
     * config
     * The configuration to use during processing
     *
     * @var {Config}
     */
    readonly config: Config;
    /**
     * repository
     * A repository to use for this process
     *
     * @var {ICodeRepository}
     */
    repository: ICodeRepository;
    /**
     * context
     * The data to use for the build
     *
     * @var {IContext}
     */
    context: type.IContext;
    /**
     * workspace
     * The directory that contains the working files
     *
     * @var {string}
     */
    workspace: string;
    /**
     * tasks
     * All of the tasks to run.
     *
     * @var {ITaskConstructor[]}
     */
    tasks: type.ITaskConstructor[];
    /**
     * postTasks
     * These tasks run after all the tasks and forks are ran. They are usually
     * processing the end result, like uploading packages or logs after the
     * regular tasks are finished.
     *
     * @var {ITaskConstructor[]}
     */
    postTasks: type.ITaskConstructor[];
    /**
     * forks
     * All of the forks we are going to run after the current task ends.
     *
     * @var {Worker[]}
     */
    forks: Worker[];
    /**
     * If we are currently running the worker
     *
     * @var {Workable}
     */
    running: type.ITask | null;
    /**
     * Creates a new worker process
     *
     * @param {App} app - The base App container
     * @param {ICodeRepository} repository - The repository to process on
     * @param {IContext} context - The starting context for building
     */
    constructor(app: App, repository: ICodeRepository, context: type.IContext);
    /**
     * fails
     * Checks if the worker fails
     *
     * @return {boolean}
     */
    readonly fails: boolean;
    /**
     * passes
     * Checks if the worker passes
     *
     * @return {boolean}
     */
    readonly passes: boolean;
    /**
     * runningIndex
     * Returns the currently running task index
     *
     * @return {Number}
     */
    protected readonly runningIndex: number;
    /**
     * contexts
     * Returns a list of all the contexts this worker has, and all of it's forks
     *
     * @return {IContext[]}
     */
    protected readonly contexts: type.IContext[];
    /**
     * Returns all the logs that the worker had created. Inserts some helpful
     * information like architecture, distribution, and references to the log as
     * well.
     *
     * @return {ILog[]}
     */
    protected readonly resultLogs: type.ILog[];
    /**
     * result
     * Returns the result of the worker. Possible, but incomplete if not stopped.
     *
     * @return {IResult}
     */
    readonly result: type.IResult;
    /**
     * setup
     * Creates a workspace for the process
     *
     * @async
     * @return {void}
     */
    setup(): Promise<void>;
    /**
     * run
     * Runs a bun of tasks. The first param is do match the ITask.
     *
     * @async
     */
    run(): Promise<void>;
    /**
     * fork
     * Creates a new worker, copying most of the properties from this instance.
     * It will then run all of these forks _AFTER_ the current task is done.
     *
     * @example
     *   Some tests, like setting up the workspace, can have multiple outputs. In
     *   an effort to keep things linear and _hopefully_ easy to understand the
     *   order, this is the way we would make multiple outputs possible. If the
     *   task, it will look at all the repository references and determine what
     *   kinds of packages it can make. Then, for each distribution, it forks.
     *   We end up with 3 different `Worker`s running, and on exit, merging
     *   storages.
     *
     * @async
     * @param {Object} [context] - Overwrite for the forked context
     * @return {Worker}
     */
    fork(context?: {}): Promise<Worker>;
    /**
     * teardown
     * Removes files and cleans up remaining files
     *
     * @async
     * @return {void}
     */
    teardown(): Promise<void>;
    /**
     * Adds a log/error to storage
     *
     * @param {Error} err
     * @return {Worker}
     */
    report(err: Error): this;
    /**
     * Stops the build if it's currently running
     *
     * @return {IResult}
     */
    stop(): type.IResult;
    /**
     * Given a log, we can find what context, or context of a fork the log belongs
     * to. This is useful to get more information about the log's origin like
     * architecture and distribution.
     *
     * @param {ILog} log
     * @return {IContext|null}
     */
    protected getContextForLog(log: type.ILog): type.IContext | null;
    /**
     * Adds some context information to the end of the log
     *
     * @param {type.ILog} log The log to add information to
     * @param {type.IContext[]} contexts Information to add to the log
     * @return {string} New Log body text
     */
    protected getContextLogBody(log: type.ILog, contexts: type.IContext[]): string;
}
