/**
 * houston/src/worker/log.ts
 * A log to be passed around during a worker role
 */
import { Level } from '../lib/log/level';
import { ILog } from './type';
export declare class Log extends Error implements ILog {
    /**
     * A handy level assignment for easier usage
     *
     * @var {Level}
     */
    static Level: typeof Level;
    /**
     * The log title
     *
     * @var {string}
     */
    title: string;
    /**
     * The log body
     *
     * @var {string|null}
     */
    body?: string;
    /**
     * The severity of the log
     *
     * @var {LogLevel}
     */
    level: Level;
    /**
     * A wrapped native error
     *
     * @var {Error|null}
     */
    error?: Error;
    /**
     * Creates a new log from a file. This will take the first non-whitespace line
     * as the title, and the rest as the Log body
     *
     * @param {Level} level
     * @param {string} path
     * @param {object} [data]
     * @return {Log}
     */
    static template(level: Level, path: string, data?: {}): Log;
    /**
     * Creates a new Log
     *
     * @param {Level} level
     * @param {string} title
     * @param {string} [body]
     */
    constructor(level: Level, title: string, body?: string);
    /**
     * Wraps an error in the current Log
     *
     * @param {Error} error
     * @return {Log}
     */
    setError(error: Error): Log;
    /**
     * Returns a nice string version of the log
     *
     * @return {string}
     */
    toString(): string;
}
