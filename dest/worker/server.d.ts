/**
 * houston/src/worker/server.ts
 * A server to listen for build requests
 *
 * @exports {Class} Server
 */
import { Repository } from '../lib/service/base/repository';
import { Storable } from './type';
export declare class Server {
    start(): Promise<void>;
    stop(): Promise<void>;
    protected branches(repository: Repository): Promise<void>;
    protected handleBuildRequest(storable: Storable): Promise<void>;
}
