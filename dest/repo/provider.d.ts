/**
 * houston/src/repo/provider.ts
 * Provides the app with the needed Repo server classes
 */
import { ContainerModule } from 'inversify';
export declare const provider: ContainerModule;
