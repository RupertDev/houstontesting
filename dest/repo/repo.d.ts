/**
 * houston/src/repo/repo.ts
 * Entry point for repository syslog server. I whould highly recommend reading
 * some of the code from the node syslogd package and the dgram docs.
 *
 * TODO: We should cache download count for a while so we can mass increment
 *
 * @exports {Class} Repo - A repository syslog server
 */
/// <reference types="node" />
import * as dgram from 'dgram';
import { Config } from '../lib/config';
import { Database } from '../lib/database/database';
import { Logger } from '../lib/log';
/**
 * Repo
 * A repository syslog server. Tries to mirror the Server class methods.
 *
 * @property {Socket} server
 * @property {number} port
 */
export declare class Repo {
    server: dgram.Socket;
    port: number;
    protected config: Config;
    protected database: Database;
    protected logger: Logger;
    /**
     * Creates a new web server
     *
     * @param {Config} config - The configuration to use
     * @param {Database} database - The database connection to use
     * @param {Logger} logger - The log instance to use
     */
    constructor(config: Config, database: Database, logger: Logger);
    /**
     * onError
     * Handles a download message from web server
     *
     * @param {Error} err - An error that occured
     *
     * @return {void}
     */
    onError(err: Error): void;
    /**
     * onMessage
     * Handles a download message from web server
     *
     * @async
     * @param {Buffer} buf - The message sent from the web server
     *
     * @return {void}
     */
    onMessage(buf: Buffer): Promise<void>;
    /**
     * listen
     * Starts the repository syslogd server
     *
     * @async
     * @param {number} [port] - A port to listen on. Kept for backwards support
     *
     * @throws {Error} - When unable to listen to requested port
     * @return {Server} - An active Server class
     */
    listen(port?: number): Promise<this>;
    /**
     * close
     * Stops the syslog server
     *
     * @async
     *
     * @throws {Error} - When the Server class is messed up
     * @return {Server} - An inactive Server class
     */
    close(): Promise<this>;
}
